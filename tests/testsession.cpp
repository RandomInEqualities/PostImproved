#include "testsession.h"

#include <QtTest>

void TestSession::testGet()
{
    initialize();
    network->expect(NetworkAccessMonitor::GetOperation, getExpectedUrl());
    session->getResource(eboks::Url(path));
}

void TestSession::testPut()
{
    initialize();
    network->expect(NetworkAccessMonitor::PutOperation, getExpectedUrl());
    session->putResource(eboks::Url(path));
}

void TestSession::testPutWithBody()
{
    initialize();
    QByteArray body = "testbody";
    network->expect(NetworkAccessMonitor::PutOperation, getExpectedUrl(), body);
    session->putResource(eboks::Url(path), body, "testtype");
}

void TestSession::testDelete()
{
    initialize();
    network->expect(NetworkAccessMonitor::DeleteOperation, getExpectedUrl());
    session->deleteResource(eboks::Url(path));
}

void TestSession::initialize()
{
    delete session;
    delete network;

    network = new NetworkAccessMonitor(this);
    network->setNetworkAccessible(QNetworkAccessManager::NotAccessible);

    session = new eboks::Session(this);
    session->fakeLogin(username, userid);
    session->setNetwork(network);
}

QString TestSession::getExpectedUrl() const
{
    return "https://rest.e-boks.dk/mobile/1/xml.svc/en-gb/" + userid + "/" + path;
}
