#include "testdatastructures.h"

#include <QtTest>

#include "eboks/folder.h"
#include "eboks/id.h"
#include "eboks/message.h"
#include "eboks/sender.h"

void TestDataStructures::initTestCase()
{
}

void TestDataStructures::cleanupTestCase()
{
}

void TestDataStructures::testFolder()
{
    eboks::Folder::Data data;

    eboks::FolderId folderId("123");
    QString folderName("Name");
    eboks::FolderId parentId("456");
    QVector<eboks::FolderId> subfolderIds;
    subfolderIds.append(eboks::FolderId("Subforlder 1 Id"));
    subfolderIds.append(eboks::FolderId("Subforlder 2 Id"));
    eboks::Folder::Type type = eboks::Folder::Type::DeletedItems;
    int unreadAmount = 11;

    data.id = folderId;
    data.name = folderName;
    data.parentId = parentId;
    data.subfolderIds = subfolderIds;
    data.type = type;
    data.unreadAmount = unreadAmount;
    eboks::Folder folder(data);

    QCOMPARE(folder.getId(), folderId);
    QCOMPARE(folder.getName(), folderName);
    QCOMPARE(folder.getParentId(), parentId);
    QCOMPARE(folder.getSubfolderIds().size(), subfolderIds.size());
    QCOMPARE(folder.getSubfolderIds().at(0), subfolderIds.at(0));
    QCOMPARE(folder.getSubfolderIds().at(1), subfolderIds.at(1));
    QCOMPARE(folder.getType(), type);
    QCOMPARE(folder.getUnreadAmount(), unreadAmount);
}

void TestDataStructures::testMessage()
{

    eboks::Message::Data data;

    int attachmentCount = 2;
    int filesize = 122;
    QString filetype("pdf");
    eboks::FolderId folderId("456");
    bool isUnread = true;
    eboks::MessageId messageId("123");
    QString note = "This is a note";
    QDateTime receiveTime(QDateTime::currentDateTime());

    QString senderName("sender name");
    eboks::SenderId senderId("sender id");
    eboks::Sender sender(senderName, senderId);

    QString title("message title");

    data.attachmentCount = attachmentCount;
    data.filesize = filesize;
    data.filetype = filetype;
    data.folderId = folderId;
    data.id = messageId;
    data.isUnread = isUnread;
    data.note = note;
    data.receiveTime = receiveTime;
    data.sender = sender;
    data.title = title;

    eboks::Message message(data);

    QCOMPARE(message.getAttachmentCount(), attachmentCount);
    QCOMPARE(message.getFileSize(), filesize);
    QCOMPARE(message.getFileType(), filetype);
    QCOMPARE(message.getFolderId(), folderId);
    QCOMPARE(message.getId(), messageId);
    QCOMPARE(message.getNote(), note);
    QCOMPARE(message.getReceiveTime(), receiveTime);
    QCOMPARE(message.getSender().getId(), senderId);
    QCOMPARE(message.getSender().getName(), senderName);
    QCOMPARE(message.getTitle(), title);
}

void TestDataStructures::testFolderModel()
{
}
