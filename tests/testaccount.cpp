#include "testaccount.h"

#include <QDate>
#include <QDateTime>
#include <QTemporaryFile>
#include <QtTest>

void TestAccount::testGetFolders()
{
    initialize();
    QUrl expected = QUrl(getRoot() + "/0/mail/folders");
    network->expect(NetworkAccessMonitor::GetOperation, expected);
    account->getFolders();
}

void TestAccount::testCreateFolder()
{
    initialize();
    QUrl expected = QUrl(getRoot() + "/0/mail/folder/" + folderId.toString() + "?folderName=" + name);
    network->expect(NetworkAccessMonitor::PutOperation, expected);
    account->createFolder(name, folderId);
}

void TestAccount::testDeleteFolder()
{
    initialize();
    QUrl expected = QUrl(getRoot() + "/0/mail/folder/" + folderId.toString());
    network->expect(NetworkAccessMonitor::DeleteOperation, expected);
    account->deleteFolder(folderId);
}

void TestAccount::testUpdateFolder()
{
    eboks::FolderId toFolderId = eboks::FolderId("1234");
    QUrl expected = QUrl(getRoot() + "/0/mail/folder/" + folderId.toString() + "?newFolderName=" + name + "&"
        + "toFolderId=" + toFolderId.toString());
    network->expect(NetworkAccessMonitor::PutOperation, expected);
    account->updateFolder(folderId, toFolderId, name);
}

void TestAccount::testGetSenders()
{
    initialize();
    QUrl expected = QUrl(getRoot() + "/0/mail/folders/search/senders");
    network->expect(NetworkAccessMonitor::GetOperation, expected);
    account->getSenders();
}

void TestAccount::testGetLatest()
{
    initialize();
    QUrl expected = QUrl(getRoot() + "/0/mail/folder/search/latest");
    network->expect(NetworkAccessMonitor::GetOperation, expected);
    account->getLatest();
}

void TestAccount::testGetUnread()
{
    initialize();
    int skip = 5;
    int take = 6;
    QUrl expected = QUrl(
        getRoot() + "/0/mail/folder/search/unread?skip=" + QString::number(skip) + "&take=" + QString::number(take));
    network->expect(NetworkAccessMonitor::GetOperation, expected);
    account->getUnread(skip, take);
}

void TestAccount::testGetBetweenDates()
{
    initialize();
    QDateTime start = QDateTime(QDate(2011, 1, 1));
    QDateTime end = QDateTime(QDate(2012, 1, 1));
    QUrl expected = QUrl(getRoot() + "/0/mail/folder/search/latest?from=" + start.toString(Qt::ISODate) + "&to="
        + end.toString(Qt::ISODate));
    network->expect(NetworkAccessMonitor::GetOperation, expected);
    account->getBetweenDates(start, end);
}

void TestAccount::testGetFromFolder()
{
    initialize();
    int skip = 5;
    int take = 6;
    QUrl expected = QUrl(getRoot() + "/0/mail/folder/" + folderId.toString() + "?skip=" + QString::number(skip)
        + "&take=" + QString::number(take));
    network->expect(NetworkAccessMonitor::GetOperation, expected);
    account->getFromFolder(folderId, skip, take);
}

void TestAccount::testGetFromSender()
{
    initialize();
    int skip = 5;
    int take = 6;
    QUrl expected = QUrl(getRoot() + "/0/mail/folder/search/senders/" + senderId.toString() + "?skip="
        + QString::number(skip) + "&take=" + QString::number(take));
    network->expect(NetworkAccessMonitor::GetOperation, expected);
    account->getFromSender(senderId, skip, take);
}

void TestAccount::testGetFromSearch()
{
    initialize();
    int skip = 5;
    int take = 6;
    QUrl expected = QUrl(getRoot() + "/0/mail/folder/search?search=bla" + "&skip=" + QString::number(skip) + "&take="
        + QString::number(take));
    network->expect(NetworkAccessMonitor::GetOperation, expected);
    account->getFromSearch("bla", skip, take);
}

void TestAccount::testGetMessageContent()
{
    initialize();
    QUrl expected =
        QUrl(getRoot() + "/0/mail/folder/" + folderId.toString() + "/message/" + messageId.toString() + "/content");
    network->expect(NetworkAccessMonitor::GetOperation, expected);
    account->getMessageContent(folderId, messageId);
}

void TestAccount::testMoveMessage()
{
    initialize();
    QUrl expected = QUrl(
        getRoot() + "/0/mail/folder/" + folderId.toString() + "/message/" + messageId.toString() + "?toFolderId=1243");
    network->expect(NetworkAccessMonitor::PutOperation, expected);
    account->moveMessage(folderId, messageId, eboks::FolderId("1243"));
}

void TestAccount::testDeleteMessage()
{
    initialize();
    QUrl expected = QUrl(getRoot() + "/0/mail/folder/" + folderId.toString() + "/message/" + messageId.toString());
    network->expect(NetworkAccessMonitor::DeleteOperation, expected);
    account->deleteMessage(folderId, messageId);
}

void TestAccount::testSetMessageTitle()
{
    initialize();
    QUrl expected = QUrl(
        getRoot() + "/0/mail/folder/" + folderId.toString() + "/message/" + messageId.toString() + "?newName=" + name);
    network->expect(NetworkAccessMonitor::PutOperation, expected);
    account->setMessageTitle(folderId, messageId, name);
}

void TestAccount::testSetMessageNote()
{
    initialize();
    QUrl expected = QUrl(
        getRoot() + "/0/mail/folder/" + folderId.toString() + "/message/" + messageId.toString() + "?note=" + name);
    network->expect(NetworkAccessMonitor::PutOperation, expected);
    account->setMessageNote(folderId, messageId, name);
}

void TestAccount::testSetMessageRead()
{
    initialize();
    QUrl expected =
        QUrl(getRoot() + "/0/mail/folder/" + folderId.toString() + "/message/" + messageId.toString() + "?read=false");
    network->expect(NetworkAccessMonitor::PutOperation, expected);
    account->setMessageRead(folderId, messageId, false);
}

void TestAccount::testGetAttachments()
{
    initialize();
    QUrl expected = QUrl(getRoot() + "/0/mail/folder/" + folderId.toString() + "/message/" + messageId.toString());
    network->expect(NetworkAccessMonitor::GetOperation, expected);
    account->getAttachments(folderId, messageId);
}

void TestAccount::testGetAttachmentContent()
{
    initialize();
    QUrl expected =
        QUrl(getRoot() + "/0/mail/folder/" + folderId.toString() + "/message/" + attachmentId.toString() + "/content");
    network->expect(NetworkAccessMonitor::GetOperation, expected);
    account->getAttachmentContent(folderId, attachmentId);
}

void TestAccount::testUploadMessage()
{
    initialize();
    QUrl expected = QUrl(getUploadUrl());
    QByteArray content = "testcontent";
    network->expect(NetworkAccessMonitor::PutOperation, expected, content);
    account->uploadMessage(folderId, name, QString(content), filetype);
}

void TestAccount::testUploadFile()
{
    initialize();

    QTemporaryFile file;
    if (!file.open()) {
        QFAIL("file not open");
    }

    QByteArray content = "testcontent";
    file.write(content);
    file.close();

    QUrl expected = QUrl(getUploadUrl());
    network->expect(NetworkAccessMonitor::PutOperation, expected, content);
    account->uploadFile(folderId, name, file.fileName(), filetype);
}

void TestAccount::testUploadBytes()
{
    initialize();
    QUrl expected = QUrl(getUploadUrl());
    QByteArray content = "testcontent";
    network->expect(NetworkAccessMonitor::PutOperation, expected, content);
    account->uploadBytes(folderId, name, content, filetype);
}

void TestAccount::initialize()
{
    delete account;
    delete session;
    delete network;

    network = new NetworkAccessMonitor(this);
    network->setNetworkAccessible(QNetworkAccessManager::NotAccessible);

    session = new eboks::Session(this);
    session->fakeLogin(username, userid);
    session->setNetwork(network);

    account = new eboks::Account(session);
}

QString TestAccount::getRoot() const
{
    return "https://rest.e-boks.dk/mobile/1/xml.svc/en-gb/" + userid;
}

QString TestAccount::getUploadUrl() const
{
    return getRoot() + "/0/mail/folder/" + folderId.toString() + "?name=" + name + "&filetype=" + filetype;
}
