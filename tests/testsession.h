#pragma once

#include "networkaccessmonitor.h"

#include "eboks/session.h"

#include <QObject>
#include <QString>

class TestSession : public QObject
{
private:
    Q_OBJECT

private slots:

    void testGet();
    void testPut();
    void testPutWithBody();
    void testDelete();

private:
    void initialize();

    QString getExpectedUrl() const;

    eboks::Session* session = nullptr;
    NetworkAccessMonitor* network = nullptr;

    QString path = "testpath";
    QString username = "testname";
    QString userid = "1234";
};
