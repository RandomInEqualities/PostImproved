#include "testaccount.h"
#include "testdatastructures.h"
#include "testsession.h"

#include <QtTest>

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    QApplication::setApplicationName("TestApp");
    QApplication::setApplicationVersion("1.0.0");
    QApplication::setOrganizationName("PostWorks");
    Q_UNUSED(app);

    int result = 0;

    TestDataStructures testDS;
    TestSession testSession;
    TestAccount testAccount;

    result += QTest::qExec(&testDS, argc, argv);
    result += QTest::qExec(&testSession, argc, argv);
    result += QTest::qExec(&testAccount, argc, argv);

    return result;
}
