#include "networkaccessmonitor.h"

#include <QtTest>

NetworkAccessMonitor::NetworkAccessMonitor(QObject* parent) : QNetworkAccessManager(parent), haveExpectation(false)
{
}

void NetworkAccessMonitor::expect(QNetworkAccessManager::Operation operation, const QUrl& url, QByteArray body)
{
    this->haveExpectation = true;
    this->expectedOperation = operation;
    this->expectedUrl = url;
    this->expectedBody = body;
}

QNetworkReply* NetworkAccessMonitor::createRequest(
    QNetworkAccessManager::Operation operation, const QNetworkRequest& request, QIODevice* data)
{
    if (haveExpectation) {
        Q_ASSERT(expectedOperation == operation);
        Q_ASSERT(expectedUrl == request.url());

        if (data == nullptr) {
            Q_ASSERT(this->expectedBody.isEmpty());
        }
        else {
            Q_ASSERT(this->expectedBody == data->readAll());
            data->seek(0);
        }
    }

    return QNetworkAccessManager::createRequest(operation, request, data);
}
