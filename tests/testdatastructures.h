#pragma once

#include <QObject>
#include <QString>

class TestDataStructures : public QObject
{
private:
    Q_OBJECT

private slots:

    void initTestCase();
    void cleanupTestCase();

    void testFolder();
    void testMessage();
    void testFolderModel();
};
