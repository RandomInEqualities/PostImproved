#pragma once

#include "networkaccessmonitor.h"

#include "eboks/account.h"
#include "eboks/attachment.h"
#include "eboks/folder.h"
#include "eboks/message.h"
#include "eboks/sender.h"
#include "eboks/session.h"

#include <QObject>
#include <QString>

class TestAccount : public QObject
{
private:
    Q_OBJECT

private slots:

    void testGetFolders();
    void testCreateFolder();
    void testDeleteFolder();
    void testUpdateFolder();

    void testGetSenders();

    void testGetLatest();
    void testGetUnread();
    void testGetBetweenDates();
    void testGetFromFolder();
    void testGetFromSender();
    void testGetFromSearch();

    void testGetMessageContent();

    void testMoveMessage();
    void testDeleteMessage();
    void testSetMessageTitle();
    void testSetMessageNote();
    void testSetMessageRead();

    void testGetAttachments();
    void testGetAttachmentContent();

    void testUploadMessage();
    void testUploadFile();
    void testUploadBytes();

private:
    void initialize();

    QString getRoot() const;
    QString getUploadUrl() const;

    eboks::Session* session = nullptr;
    eboks::Account* account = nullptr;
    NetworkAccessMonitor* network = nullptr;

    const QString username = "testname";
    const QString userid = "1234";

    const QString name = "testname";
    const QString filetype = "tfg";
    const eboks::FolderId folderId = eboks::FolderId("12534");
    const eboks::MessageId messageId = eboks::MessageId("24");
    const eboks::AttachmentId attachmentId = eboks::AttachmentId("62");
    const eboks::SenderId senderId = eboks::SenderId("79345");
};
