# This file compiles the eboks application tests.

QT += core gui widgets network webenginewidgets testlib
CONFIG += c++14 testcase

TARGET = EboksTests
TEMPLATE = app

INCLUDEPATH += ../eboks
DEPENDPATH += ../eboks
LIBS += -L../eboks -leboks

SOURCES += \
    main.cpp \
    testdatastructures.cpp \
    testsession.cpp \
    networkaccessmonitor.cpp \
    testaccount.cpp

HEADERS += \
    testdatastructures.h \
    testsession.h \
    networkaccessmonitor.h \
    testaccount.h

# If the eboks or gui library changes we need to recreate the executable. Qmake
# does not do this automatically, we need to tell where the library is.
win32{
    PRE_TARGETDEPS += ../eboks/eboks.lib
}
unix{
    PRE_TARGETDEPS += ../eboks/libeboks.a
}
