#pragma once

#include <QIODevice>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

class NetworkAccessMonitor : public QNetworkAccessManager
{
public:
    NetworkAccessMonitor(QObject* parent = nullptr);

    // Test that the next network request is the given operation, url and body.
    void expect(Operation op, const QUrl& expectedUrl, QByteArray expectedBody = QByteArray());

protected:
    virtual QNetworkReply* createRequest(
        Operation op, const QNetworkRequest& req, QIODevice* outgoingData = Q_NULLPTR) override;

private:
    bool haveExpectation;

    Operation expectedOperation;
    QUrl expectedUrl;
    QByteArray expectedBody;
};
