# PostImproved

This is an application for viewing and organising Danish Digital Post. It 
works on Windows, Linux and OSX.

We have attempted to make an application that is easier to use than the 
e-Boks website. 

# Project Structure

/project.pro - the main qmake build file.
/app - the app executable directory.
/eboks - the eboks API binding directory. Builds into a library.
/gui - the user interface directory. Builds into a library.
/tests - the test executable directory. 

# How To Build

Our project depends on the Qt framework. It requires

1. A Qt 5.9+ installation with QWebEngine.
2. The qmake build system.
3. An OpenSSL 1.0.1 installation.
3. A C++14 compiler.

We have used the Qt Creator IDE to build the project. But it is possible 
to build manually by invoking qmake and the C++ compiler:

1. cd PostImproved
2. qmake project.pro
3. make (requires make)