# This project file compiles the eboks application and its tests.

# The project is split into 4 parts:
#
# - the eboks directory where we have an eboks api binding. It is built as a
#   static library.
#
# - the gui directory where we have the gui classes. It is built as a static
#   library.
#
# - the app directory where the entry point of the application is. It is
#   built as an executable and links to the eboks and gui libraries.
#
# - the tests directory where the tests for the application classes is. It is
#   built as an executable and links to the eboks and gui libraries.

TEMPLATE = subdirs

SUBDIRS = \
    eboks \
    gui \
    app \
    tests

gui.depends = eboks
app.depends = eboks gui
tests.depends = eboks
