# This file compiles the eboks application.

QT += core gui widgets network webenginewidgets
CONFIG += c++14

TARGET = EboksApp
TEMPLATE = app

INCLUDEPATH += ../eboks ../gui
DEPENDPATH += ../eboks ../gui
LIBS += -L../eboks -L../gui -lgui -leboks

SOURCES += \
    main.cpp

# If the eboks or gui library changes we need to recreate the executable. Qmake
# does not do this automatically, we need to tell where the library is.
win32{
    PRE_TARGETDEPS += ../eboks/eboks.lib ../gui/gui.lib
}
unix{
    PRE_TARGETDEPS += ../eboks/libeboks.a ../gui/libgui.a
}
