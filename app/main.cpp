#include "app_controller.h"

#include <QApplication>
#include <QLocale>
#include <QSettings>

int main(int argc, char* argv[])
{
    QLocale danish(QLocale::Danish, QLocale::Denmark);
    QLocale::setDefault(danish);

    // Workaround rendering issues with Intel graphics cards and OpenGL rendering.
    // (On some Dell machines widgets using OpenGL gets rendered in wrong position)
    // Please remove this some time in the future.
    QApplication::setAttribute(Qt::AA_UseOpenGLES);

    QApplication app(argc, argv);
    QApplication::setApplicationName("PostImproved");
    QApplication::setApplicationVersion("1.0.0");
    QApplication::setOrganizationName("PostWorks");
    QSettings::setDefaultFormat(QSettings::IniFormat);

    AppController window;
    window.show();

    return app.exec();
}
