# This file compiles the eboks source files into a static library.

QT += core network
CONFIG += c++14 static

TARGET = eboks
TEMPLATE = lib
DESTDIR = $$OUT_PWD

SOURCES += \
    eboks/account.cpp \
    eboks/attachment.cpp \
    eboks/folder.cpp \
    eboks/id.cpp \
    eboks/message.cpp \
    eboks/request.cpp \
    eboks/sender.cpp \
    eboks/session.cpp \
    eboks/url.cpp \
    eboks/deletion_manager.cpp \
    eboks/downlad_manager.cpp

HEADERS += \
    eboks/account.h \
    eboks/attachment.h \
    eboks/folder.h \
    eboks/id.h \
    eboks/message.h \
    eboks/request.h \
    eboks/sender.h \
    eboks/session.h \
    eboks/url.h \
    eboks/deletion_manager.h \
    eboks/downlad_manager.h

