#include "eboks/message.h"

namespace eboks
{

Message::Message() : m_data()
{
}

Message::Message(const Message::Data& data) : m_data(data)
{
}

bool Message::operator==(const Message& message) const
{
    return m_data.id == message.m_data.id;
}

bool Message::operator!=(const Message& message) const
{
    return !operator==(message);
}

QString Message::getTitle() const
{
    return m_data.title;
}

MessageId Message::getId() const
{
    return m_data.id;
}

FolderId Message::getFolderId() const
{
    return m_data.folderId;
}

bool Message::isUnread() const
{
    return m_data.isUnread;
}

Sender Message::getSender() const
{
    return m_data.sender;
}

QDateTime Message::getReceiveTime() const
{
    return m_data.receiveTime;
}

int Message::getFileSize() const
{
    return m_data.filesize;
}

QString Message::getFileType() const
{
    return m_data.filetype;
}

int Message::getAttachmentCount() const
{
    return m_data.attachmentCount;
}

QString Message::getNote() const
{
    return m_data.note;
}

bool Message::isUploaded() const
{
    return m_data.sender.getId() == eboks::userId;
}

Message::Data Message::getData() const
{
    return m_data;
}

} // namespace eboks
