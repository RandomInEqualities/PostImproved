#include "eboks/deletion_manager.h"

#include "eboks/request.h"

#include <QXmlStreamReader>

namespace eboks
{

DeletionManager::DeletionManager(eboks::Account* account)
    : QObject(account), m_account(account), m_haveDeletionFolder(false)
{
    // Attempt to create the folder that we put deleted items in.
    m_account->createFolder(deletionFolderName, eboks::rootId);

    // Find the folder (we have either created it or it was already created).
    auto request = m_account->getFolders();
    connect(request.get(), &eboks::Request::finished, this, &DeletionManager::findDeletionFolder);
}

const Folder& DeletionManager::getDeletionFolder()
{
    return m_deletionFolder;
}

void DeletionManager::deleteMessage(const eboks::Message& message)
{
    if (!m_haveDeletionFolder) {
        return;
    }

    if (message.getFolderId() == m_deletionFolder.getId()) {
        // The message have already been deleted and the user is trying to
        // delete it from the backup folder. Allow deletion if it was uploaded
        // by the user.
        if (message.isUploaded()) {
            m_account->deleteMessage(message.getFolderId(), message.getId());
        }
    }
    else {
        // Move the message to the deletion folder.
        m_account->moveMessage(message.getFolderId(), message.getId(), m_deletionFolder.getId());
    }
}

void DeletionManager::deleteFolder(const eboks::Folder& folder)
{
    if (!m_haveDeletionFolder) {
        return;
    }
    if (!folder.isCustomFolder()) {
        return;
    }
    if (folder.getId() == m_deletionFolder.getId()) {
        return;
    }

    // Check if the folder is empty.
    auto request = m_account->getFromFolder(folder.getId(), 0, 0);
    connect(request.get(), &eboks::Request::finished, this,
        [this, folder](QNetworkReply* reply) { findFolderMessageAmount(reply, folder); });
}

void DeletionManager::findDeletionFolder(QNetworkReply* reply)
{
    if (reply->error()) {
        return;
    }

    QVector<eboks::Folder> folders = m_account->parseFoldersXml(reply);

    for (const auto& folder : folders) {
        if (folder.getParentId() != eboks::rootId) {
            continue;
        }

        if (folder.getType() != eboks::Folder::Type::CustomFolder) {
            continue;
        }

        if (folder.getName() != deletionFolderName) {
            continue;
        }

        m_deletionFolder = folder;
        m_haveDeletionFolder = true;
        return;
    }
}

void DeletionManager::findFolderMessageAmount(QNetworkReply* reply, const eboks::Folder& folder)
{
    // Find the amount of messages in the folder by parsing the messages
    // attribute in the reply.
    QXmlStreamReader xml(reply);
    int messagesInFolder = -1;

    while (!xml.atEnd()) {
        xml.readNext();

        if (xml.isStartElement() && xml.name() == "Folder") {
            Q_ASSERT(xml.attributes().hasAttribute("messages"));
            messagesInFolder = xml.attributes().value("messages").toInt();
            break;
        }
    }

    if (xml.error() || messagesInFolder < 0) {
        Q_ASSERT(false);
        return;
    }

    deleteFolder(folder, messagesInFolder);
}

void DeletionManager::deleteFolder(const Folder& folder, int messagesInFolder)
{
    if (!m_haveDeletionFolder) {
        return;
    }
    if (!folder.isCustomFolder()) {
        return;
    }
    if (folder.getId() == m_deletionFolder.getId()) {
        return;
    }

    if (messagesInFolder == 0) {
        // The folder is empty, we can delete it.
        m_account->deleteFolder(folder.getId());
    }
    else {
        // The folder is not empty, move it to the deletion folder. Folders
        // can not have the same names, so we add the time to the folder.
        if (folder.getParentId() == m_deletionFolder.getId()) {
            return;
        }

        qint64 timeSinceEpoch = QDateTime::currentMSecsSinceEpoch();
        m_account->updateFolder(
            folder.getId(), m_deletionFolder.getId(), folder.getName() + "_" + QString::number(timeSinceEpoch));
    }
}

} // namespace eboks
