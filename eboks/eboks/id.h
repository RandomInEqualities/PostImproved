#pragma once

#include <QString>

namespace eboks
{

class FolderId
{
public:
    FolderId();
    FolderId(QString id);

    QString toString() const;
    bool isValid() const;

    bool operator==(const FolderId& id) const;
    bool operator!=(const FolderId& id) const;

private:
    QString m_id;
};

class MessageId
{
public:
    MessageId();
    MessageId(QString id);

    QString toString() const;
    bool isValid() const;

    bool operator==(const MessageId& id) const;
    bool operator!=(const MessageId& id) const;

private:
    QString m_id;
};

class AttachmentId
{
public:
    AttachmentId();
    AttachmentId(QString id);

    QString toString() const;
    bool isValid() const;

    bool operator==(const AttachmentId& id) const;
    bool operator!=(const AttachmentId& id) const;

private:
    QString m_id;
};

class SenderId
{
public:
    SenderId();
    SenderId(QString id);

    QString toString() const;
    bool isValid() const;

    bool operator==(const SenderId& id) const;
    bool operator!=(const SenderId& id) const;

private:
    QString m_id;
};

// The id for the root folder in e-Boks.
const eboks::FolderId rootId = eboks::FolderId("0");

// The id for the sender that represents yourself.
const eboks::SenderId userId = eboks::SenderId("0");

// We want to use the id's in QHash, which needs some global functions defined.
uint qHash(const eboks::FolderId& id, uint seed = 0);
uint qHash(const eboks::MessageId& id, uint seed = 0);
uint qHash(const eboks::SenderId& id, uint seed = 0);
uint qHash(const eboks::AttachmentId& id, uint seed = 0);

} // namespace eboks
