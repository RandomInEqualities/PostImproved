#pragma once

#include "eboks/url.h"

#include <QByteArray>
#include <QNetworkReply>
#include <QObject>
#include <QString>

namespace eboks
{

// The Request class represents an eboks api request.
class Request : public QObject
{
private:
    Q_OBJECT

public:
    // The HTTP request methods.
    enum class Method
    {
        GET,
        PUT,
        DELETE
    };

    // The request types. Used to set the correct url and authentication
    // header.
    enum class Type
    {
        LOGIN,
        LOGOUT,
        QUERY
    };

    // Create the request.
    Request(Method method, Type type, Url url, QObject* parent = nullptr);

    // Get the method.
    Method getMethod() const;

    // Get the type.
    Type getType() const;

    // Get the url.
    Url getUrl() const;

    // Get the content body. It can be empty.
    const QByteArray& getContent() const;

    // Get the content type. It can be empty.
    const QByteArray& getContentType() const;

    // Set the content body and type for the request.
    void setContent(const QByteArray& content, const QByteArray& contentType);

signals:

    // Signal emitted when the response to this request is received. This class
    // does not emit or send the HTTP request, an external class must do it.
    // This signal is emitted when a external class emits is. Requests coming
    // from the Account and Session classes will emit this signal.
    void finished(QNetworkReply* reply);

private:
    Method m_method;
    Type m_type;
    Url m_url;
    QByteArray m_content;
    QByteArray m_contentType;
};

} // namespace eboks
