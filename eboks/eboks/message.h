#pragma once

#include "eboks/id.h"
#include "eboks/sender.h"

#include <QDateTime>
#include <QString>

namespace eboks
{

// The Message class holds the metadata of an eboks message.
class Message
{
public:
    // Thin wrapper around the metadata.
    struct Data
    {
        QString title;
        MessageId id;
        QDateTime receiveTime;
        FolderId folderId;
        bool isUnread;
        Sender sender;
        int filesize;
        QString filetype;
        int attachmentCount;
        QString note;
    };

public:
    // Create message with invalid data.
    Message();

    // Create message with specified data.
    Message(const Data& data);

    // See if two messages are the same. Compares the id's.
    bool operator==(const Message& message) const;
    bool operator!=(const Message& message) const;

    // Get the message title.
    QString getTitle() const;

    // Get the unique message identification number.
    MessageId getId() const;

    // The id for the folder that the message is in.
    FolderId getFolderId() const;

    // Is the message marked read?
    bool isUnread() const;

    // The one who sent the message.
    Sender getSender() const;

    // The time the message was received.
    QDateTime getReceiveTime() const;

    // The size in bytes of the message content.
    int getFileSize() const;

    // The type of message content (pdf, html, txt, etc.).
    QString getFileType() const;

    // The amount of attachments in the message.
    int getAttachmentCount() const;

    // The message note (can be empty).
    QString getNote() const;

    // Is the message uploaded by the user?
    bool isUploaded() const;

    // All data for the message.
    Data getData() const;

private:
    Data m_data;
};

} // namespace eboks
