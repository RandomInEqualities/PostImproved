#include "eboks/url.h"

#include <QtGlobal>

namespace eboks
{

Url::Url(QString path) : m_path(path)
{
}

QUrl Url::build() const
{
    Q_ASSERT(!m_path.isEmpty());
    Q_ASSERT(!m_root.isEmpty());

    QString fullpath;
    if (m_userid.isEmpty()) {
        fullpath = m_root + "/" + m_path;
    }
    else {
        fullpath = m_root + "/" + m_userid + "/" + m_path;
    }

    QUrl url(fullpath);
    url.setQuery(m_query);
    return url;
}

void Url::setUserid(QString userid)
{
    m_userid = userid;
}

void Url::setPath(QString path)
{
    m_path = path;
}

void Url::addQuery(QString name, QString value)
{
    m_query.addQueryItem(name, value);
}

void Url::addQuery(QString name, int value)
{
    addQuery(name, QString::number(value));
}

QString Url::getRoot() const
{
    return m_root;
}

QString Url::getUserid() const
{
    return m_userid;
}

QString Url::getPath() const
{
    return m_path;
}

QUrlQuery Url::getQuery() const
{
    return m_query;
}

} // namespace eboks
