#pragma once

#include "eboks/account.h"
#include "eboks/folder.h"
#include "eboks/message.h"

#include <QObject>
#include <QString>

namespace eboks
{

// This class manages downloads of e-Boks folders and messages. It will
// download into a directory. The download progress can be queried with
// isDownload such that we for example don't close the application while a
// download is in progress.
class DownloadManager : public QObject
{
private:
    Q_OBJECT

public:
    DownloadManager(eboks::Account* account);

    // Download a message into a directory. The filename will be the message
    // title.
    void downloadMessage(const eboks::Message& message, QDir directory);

    // Download a folder into a directory. The message filenames will be the
    // message titles.
    void downloadFolder(const eboks::Folder& folder, QDir directory);

    // Is there currently a download in progress?
    bool isDownloading();

private slots:

    // Save a message's content to disk.
    void saveMessageContent(QNetworkReply* reply, const eboks::Message& message, QDir directory);

    // Download messages from a folder, skipping over skip messages when
    // requesting message metadate for the folder.
    void downloadFolder(QNetworkReply* reply, const eboks::Folder& folder, QDir directory, int skip);

    // Start downloading a folder and its subfolders in eboks. The reply must
    // be from a call to getFolders in account.
    void startFoldersDownload(QNetworkReply* reply, const eboks::Folder& folder, QDir directory);

private:
    // Save messages in a reply to disk. Fetches the messages contents.
    void saveMessages(QNetworkReply* reply, QDir directory);

    // Find a suitable file name. Escaping platform dependent characters and
    // makes sure that the file do not overwrite another file.
    QString findSuitableFileName(QDir directory, QString name, QString extension) const;

    // Find a suitable directory name inside another directory. Escaping
    // platform dependent characters and creates the directory if it does not
    // exists.
    bool findSuitableDirectory(QDir& directory, QString name) const;

    // Escape a string to exclude characters not allowed in filenames/folder
    // names.
    QString escapeName(QString filename) const;

    const int TAKE_MESSAGES = 10;
    const int MAX_FILENAME_TRIES = 512;

    eboks::Account* m_account;
    int m_downloadsInProgress;
};

} // namespace eboks
