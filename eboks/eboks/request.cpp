#include "eboks/request.h"

namespace eboks
{

Request::Request(Request::Method method, Request::Type type, Url url, QObject* parent)
    : QObject(parent), m_method(method), m_type(type), m_url(url)
{
}

Request::Method Request::getMethod() const
{
    return m_method;
}

Request::Type Request::getType() const
{
    return m_type;
}

Url Request::getUrl() const
{
    return m_url;
}

const QByteArray& Request::getContent() const
{
    return m_content;
}

const QByteArray& Request::getContentType() const
{
    return m_contentType;
}

void Request::setContent(const QByteArray& content, const QByteArray& contentType)
{
    m_content = content;
    m_contentType = contentType;
}

} // namespace eboks
