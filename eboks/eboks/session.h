#pragma once

#include "eboks/request.h"
#include "eboks/url.h"

#include <QByteArray>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>
#include <QQueue>
#include <QString>

#include <memory>

namespace eboks
{

// The Session class handles an eboks session.
class Session : public QObject
{
private:
    Q_OBJECT

public:
    Session(QObject* parent);

    // Get the username for the logged in user. Is empty if there is no
    // user logged in.
    QString getUsername();

    // Login as a specific user. The signal finished on the returned request
    // is emitted when the response is received.
    std::shared_ptr<Request> login(QString cpr, QString password, QString activationcode);

    // Log the current user out. The signal finished on the returned request
    // is emitted when the response is received.
    std::shared_ptr<Request> logout();

    // Send GET request. Sets the url userid internally. The signal finished on
    // the returned request is emitted when the response is received.
    std::shared_ptr<Request> getResource(Url url);

    // Send PUT request. Sets the url userid internally. The signal finished on
    // the returned request is emitted when the response is received.
    std::shared_ptr<Request> putResource(Url url);

    // Send PUT request with a content body. The contentType is used in the
    // content type header. Sets the url userid internally. The signal finished
    // on the returned request is emitted when the response is received.
    std::shared_ptr<Request> putResource(Url url, const QByteArray& content, const QByteArray& contentType);

    // Send DELETE request. Sets the url userid internally. The signal finished
    // on the returned request is emitted when the response is received.
    std::shared_ptr<Request> deleteResource(Url url);

public:
    // Methods used for testing.

    // Emulate a fake login, such that the class can issue network requests.
    void fakeLogin(QString username, QString userid);

    // Set the network access manager. Used for mocking it and recording
    // sent network requests.
    void setNetwork(QNetworkAccessManager* network);

signals:

    // Emitted when login succeeds.
    void loginFinished();

    // Emitted when logout succeeds.
    void logoutFinished();

    // Emitted when there is a network error in an api request.
    void error(QNetworkReply* reply, std::shared_ptr<eboks::Request> request);

private slots:

    // slot called on every network reply.
    void networkReply(QNetworkReply* reply);

private:
    // Handle a login reply.
    void finishedRequestLogin(QNetworkReply* reply);

    // Handle a logout reply.
    void finishedRequestLogout(QNetworkReply* reply);

    // Handle a query reply.
    void finishedRequestQuery(QNetworkReply* reply);

    bool isLoggedIn() const;
    QString constructLoginXml() const;

    // Extract session id and nonce from reply headers.
    void extractSessionId(QNetworkReply* reply);
    void extractNonce(QNetworkReply* reply);

    // Extract the userid and username from a reply body. After this it is
    // impossible to read the reply body again.
    void extractUserInfo(QNetworkReply* reply);

    // Get the authentication header for login and normal requests.
    QByteArray getAuthenticationLogin() const;
    QByteArray getAuthentication() const;

    // Get the authentication header challanges for login and normal requests.
    QByteArray getChallengeLogin(const QByteArray& datetime) const;
    QByteArray getChallenge() const;
    QByteArray doubleHashSha256Hex(const QByteArray& input) const;

    // Add a request to the sending queue. If the queue is empty, the requests
    // is sent immidiately.
    void enqueueRequest(std::shared_ptr<Request> request);

    // Remove the first request from the sending queue. Will send the next
    // requests if there are more.
    void dequeueRequest();

    // Send the first request in the queue. Should not be called directly,
    // only form inside dequeue and enqueue.
    void sendRequest();

    // Set the request headers that we send with every eboks api request.
    void setupCommonHeaders(QNetworkRequest& request) const;
    void setupAuthenticationHeaders(QNetworkRequest& request, Request::Type type) const;

private:
    // The network controller that all HTTP requests from this class goes
    // through.
    QNetworkAccessManager* m_network;

    // A queue of pending eboks requests. We can at most send one eboks request
    // at a time. We send a request, wait for the reply, extract the nonce in
    // the reply, send the next request with the extracted nonce and so on, do
    // this until the queue is empty. When the class receives a new request, we
    // put it at the back of the queue.
    QQueue<std::shared_ptr<Request>> m_pending;

    // The session parameters.
    QByteArray m_activation;
    QByteArray m_cpr;
    QByteArray m_password;
    QByteArray m_sessionid;
    QByteArray m_nonce;
    QString m_userid;
    QString m_username;

    // The session parameters that we keep constant. In the future we can
    // allow these to be changed.
    const QByteArray m_type = "P";
    const QByteArray m_nationality = "DK";
    const QByteArray m_deviceid = "1a50fc29-0524-45f6-9aef-f412342b389f";
};

} // namespace eboks
