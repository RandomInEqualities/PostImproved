#pragma once

#include <QString>
#include <QUrl>
#include <QUrlQuery>

namespace eboks
{

// Class that builds an eboks url.
class Url
{
public:
    // Create the url.
    Url(QString path = "");

    // Build the url to be {root}/{userid}/{path}?{queries}. If the userid is
    // not set, the url is instead built to be {root}/{path}?{queries}.
    // The root is always set to https://rest.e-boks.dk/mobile/1/xml.svc/en-gb
    // so no external queries can be made by this url.
    QUrl build() const;

    // Set the userid. Should usually be set by a session and not directly.
    void setUserid(QString userid);

    // Set the path.
    void setPath(QString path);

    // Add queries to the url.
    void addQuery(QString name, QString value);
    void addQuery(QString name, int value);

    QString getRoot() const;
    QString getUserid() const;
    QString getPath() const;
    QUrlQuery getQuery() const;

private:
    const QString m_root = "https://rest.e-boks.dk/mobile/1/xml.svc/en-gb";
    QString m_userid;
    QString m_path;
    QUrlQuery m_query;
};

} // namespace eboks
