#pragma once

#include "eboks/account.h"
#include "eboks/folder.h"
#include "eboks/message.h"

#include <QNetworkReply>
#include <QObject>
#include <QString>

namespace eboks
{

// Class that handles safe deletion of messages and folders in e-Boks. It
// will never delete messages that was sent to you, it instead moves them
// to a backup 'deletion' folder.
class DeletionManager : public QObject
{
private:
    Q_OBJECT

public:
    DeletionManager(eboks::Account* account);

    // Get the deletion folder, will return an invalid folder if the deletion
    // folder is not present.
    const eboks::Folder& getDeletionFolder();

    void deleteMessage(const eboks::Message& message);
    void deleteFolder(const eboks::Folder& folder);

private slots:

    void findDeletionFolder(QNetworkReply* reply);
    void findFolderMessageAmount(QNetworkReply* reply, const eboks::Folder& folder);

private:
    void deleteFolder(const eboks::Folder& folder, int messagesInFolder);

    const QString deletionFolderName = "Deleted Items (Backup)";

    eboks::Account* m_account;
    eboks::Folder m_deletionFolder;
    bool m_haveDeletionFolder;
};

} // namespace eboks
