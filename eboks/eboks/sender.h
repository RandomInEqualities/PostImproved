#pragma once

#include "eboks/id.h"

#include <QString>

namespace eboks
{

class Sender
{
public:
    // Create attachment with invalid data.
    Sender();

    // Create attachment with specified data.
    Sender(QString name, SenderId id);

    // See if two senders are the same. Compares the id's.
    bool operator==(const Sender& sender) const;
    bool operator!=(const Sender& sender) const;

    // Get the senders name.
    QString getName() const;

    // Get the senders id.
    SenderId getId() const;

private:
    QString m_name;
    SenderId m_id;
};

} // namespace eboks
