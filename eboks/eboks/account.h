#pragma once

#include "eboks/attachment.h"
#include "eboks/folder.h"
#include "eboks/id.h"
#include "eboks/message.h"
#include "eboks/request.h"
#include "eboks/sender.h"
#include "eboks/session.h"
#include "eboks/url.h"

#include <QDir>
#include <QNetworkReply>
#include <QObject>
#include <QVariant>
#include <QVector>
#include <QXmlStreamReader>

namespace eboks
{

class Account : public QObject
{
private:
    Q_OBJECT

public:
    struct MessageUpdateInfo
    {
        QVariant newTitle;
        QVariant newNote;
        QVariant newIsUnread;
    };

    Account(Session* session);

    // Send request to get a list of folders.
    std::shared_ptr<Request> getFolders();

    // Parse the response for a list of folders.
    QVector<Folder> parseFoldersXml(QNetworkReply* reply) const;

    // Send requests to create, delete and update folders.
    std::shared_ptr<Request> createFolder(QString name, FolderId parentId);
    std::shared_ptr<Request> deleteFolder(FolderId folderId);
    std::shared_ptr<Request> updateFolder(FolderId folderId, FolderId newParentId, QString newName);

    // Send request to get list of senders.
    std::shared_ptr<Request> getSenders();

    // Parse the reponse for a list of senders.
    QVector<Sender> parseSendersXml(QNetworkReply* reply) const;

    // Send requests to get messages.
    std::shared_ptr<Request> getLatest();
    std::shared_ptr<Request> getUnread(int skip, int take);
    std::shared_ptr<Request> getBetweenDates(QDateTime start, QDateTime end);
    std::shared_ptr<Request> getFromFolder(FolderId folderId, int skip, int take);
    std::shared_ptr<Request> getFromSender(SenderId senderId, int skip, int take);
    std::shared_ptr<Request> getFromSearch(QString search, int skip, int take);

    // Parse the response for a list of messages.
    QVector<Message> parseMessagesXml(QNetworkReply* reply) const;

    // Send request to get a message's content.
    std::shared_ptr<Request> getMessageContent(FolderId folderId, MessageId messageId);

    // Send requests to move, delete and change messages.
    std::shared_ptr<Request> moveMessage(FolderId folderId, MessageId messageId, FolderId newFolderId);
    std::shared_ptr<Request> deleteMessage(FolderId folderId, MessageId messageId);
    std::shared_ptr<Request> setMessageTitle(FolderId folderId, MessageId messageId, QString newTitle);
    std::shared_ptr<Request> setMessageNote(FolderId folderId, MessageId messageId, QString newNote);
    std::shared_ptr<Request> setMessageRead(FolderId folderId, MessageId messageId, bool newIsRead = true);

    // Send request to get information about a message, including an attachment
    // list.
    std::shared_ptr<Request> getAttachments(FolderId folderId, MessageId messageId);

    // Parse the reponse for a list of attachments.
    QVector<Attachment> parseAttachmentsXml(QNetworkReply* reply) const;

    // Send request to get the content of an attachment.
    std::shared_ptr<Request> getAttachmentContent(FolderId folderId, AttachmentId attachmentId);

    // Upload a message or file to eboks. Will appear as a regular message.
    std::shared_ptr<Request> uploadMessage(FolderId toFolderId, QString name, QString content, QString filetype);
    std::shared_ptr<Request> uploadFile(FolderId toFolderId, QString name, QString filename, QString filetype);
    std::shared_ptr<Request> uploadBytes(FolderId toFolderId, QString name, QByteArray content, QString filetype);

signals:

    // Signals emitted when a folder is changed. The QVariant parameters are
    // valid if the data that they describe have changed.
    void folderCreated(eboks::FolderId parentId);
    void folderDeleted(eboks::FolderId folderId);
    void folderUpdated(eboks::FolderId folderid, QVariant newParentId, QVariant newName);

    // Signals emitted when a message is created, deleted, moved or updated.
    void messageCreated(eboks::FolderId);
    void messageDeleted(eboks::FolderId folderId, eboks::MessageId messageId);
    void messageMoved(eboks::FolderId folderId, eboks::MessageId messageId, eboks::FolderId newFolderId);
    void messageUpdated(eboks::FolderId folderId, eboks::MessageId messageId, MessageUpdateInfo info);

private:
    Url getFolderUrl(FolderId folderId) const;
    Url getMessageUrl(FolderId folderId, MessageId messageId) const;

    bool stringToBool(QString string) const;
    QString boolToString(bool boolean) const;

    // Convert a xml FolderInfo tag to a folder. Adds itself and its subfolders
    // to the folders vector.
    Folder readFolderInfo(QXmlStreamReader& xml, FolderId parentId, QVector<Folder>& folders) const;

    // Convert a xml Sender tag to a sender.
    Sender readSender(QXmlStreamReader& xml) const;

    // Convert a xml MessageInfo tag to a message.
    Message readMessageInfo(QXmlStreamReader& xml) const;

    // Convert a xml AttachmentInfo tag to an attachment.
    Attachment readAttachmentInfo(QXmlStreamReader& xml) const;

private:
    Session* m_session;
};

} // namespace eboks
