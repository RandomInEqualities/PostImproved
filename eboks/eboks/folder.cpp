#include "eboks/folder.h"

#include <QMap>
#include <QtGlobal>

namespace eboks
{

Folder::Type Folder::typeFromString(QString string)
{
    static const QMap<QString, Type> map = {{"B", Type::Inbox}, {"K", Type::Drafts}, {"P", Type::DeletedItems},
        {"S", Type::SentItems}, {"O", Type::CustomFolder}, {"U", Type::CustomSubfolder}};

    Q_ASSERT(map.contains(string));
    return map[string];
}

Folder::Folder() : m_data()
{
}

Folder::Folder(const Folder::Data& data) : m_data(data)
{
}

bool Folder::operator==(const Folder& folder) const
{
    return m_data.id == folder.m_data.id;
}

bool Folder::operator!=(const Folder& folder) const
{
    return !operator==(folder);
}

QString Folder::getName() const
{
    return m_data.name;
}

FolderId Folder::getId() const
{
    return m_data.id;
}

int Folder::getUnreadAmount() const
{
    return m_data.unreadAmount;
}

Folder::Type Folder::getType() const
{
    return m_data.type;
}

bool Folder::isCustomFolder() const
{
    return getType() == Type::CustomFolder || getType() == Type::CustomSubfolder;
}

FolderId Folder::getParentId() const
{
    return m_data.parentId;
}

QVector<FolderId> Folder::getSubfolderIds() const
{
    return m_data.subfolderIds;
}

} // namespace eboks
