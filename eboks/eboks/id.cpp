#include "eboks/id.h"

#include <QHash>

namespace eboks
{

FolderId::FolderId()
{
}

FolderId::FolderId(QString id) : m_id(id)
{
}

QString FolderId::toString() const
{
    return m_id;
}

bool FolderId::isValid() const
{
    return !m_id.isEmpty();
}

bool FolderId::operator==(const FolderId& id) const
{
    if (m_id.isEmpty()) {
        return false;
    }
    if (id.m_id.isEmpty()) {
        return false;
    }
    return m_id == id.m_id;
}

bool FolderId::operator!=(const FolderId& id) const
{
    return !operator==(id);
}

MessageId::MessageId()
{
}

MessageId::MessageId(QString id) : m_id(id)
{
}

QString MessageId::toString() const
{
    return m_id;
}

bool MessageId::isValid() const
{
    return !m_id.isEmpty();
}

bool MessageId::operator==(const MessageId& id) const
{
    if (m_id.isEmpty()) {
        return false;
    }
    if (id.m_id.isEmpty()) {
        return false;
    }
    return m_id == id.m_id;
}

bool MessageId::operator!=(const MessageId& id) const
{
    return !operator==(id);
}

AttachmentId::AttachmentId()
{
}

AttachmentId::AttachmentId(QString id) : m_id(id)
{
}

QString AttachmentId::toString() const
{
    return m_id;
}

bool AttachmentId::isValid() const
{
    return !m_id.isEmpty();
}

bool AttachmentId::operator==(const AttachmentId& id) const
{
    if (m_id.isEmpty()) {
        return false;
    }
    if (id.m_id.isEmpty()) {
        return false;
    }
    return m_id == id.m_id;
}

bool AttachmentId::operator!=(const AttachmentId& id) const
{
    return !operator==(id);
}

SenderId::SenderId()
{
}

SenderId::SenderId(QString id) : m_id(id)
{
}

QString SenderId::toString() const
{
    return m_id;
}

bool SenderId::isValid() const
{
    return !m_id.isEmpty();
}

bool SenderId::operator==(const SenderId& id) const
{
    if (m_id.isEmpty()) {
        return false;
    }
    if (id.m_id.isEmpty()) {
        return false;
    }
    return m_id == id.m_id;
}

bool SenderId::operator!=(const SenderId& id) const
{
    return !operator==(id);
}

uint qHash(const eboks::FolderId& id, uint seed)
{
    return qHash(id.toString(), seed);
}

uint qHash(const eboks::MessageId& id, uint seed)
{
    return qHash(id.toString(), seed);
}

uint qHash(const eboks::SenderId& id, uint seed)
{
    return qHash(id.toString(), seed);
}

uint qHash(const eboks::AttachmentId& id, uint seed)
{
    return qHash(id.toString(), seed);
}

} // namespace eboks
