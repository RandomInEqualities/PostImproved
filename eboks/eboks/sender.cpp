#include "eboks/sender.h"

namespace eboks
{

Sender::Sender() : m_name(""), m_id("")
{
}

Sender::Sender(QString name, SenderId id) : m_name(name), m_id(id)
{
}

bool Sender::operator==(const Sender& sender) const
{
    return m_id == sender.m_id;
}

bool Sender::operator!=(const Sender& sender) const
{
    return !operator==(sender);
}

QString Sender::getName() const
{
    return m_name;
}

SenderId Sender::getId() const
{
    return m_id;
}

} // namespace eboks
