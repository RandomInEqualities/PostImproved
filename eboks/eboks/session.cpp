#include "eboks/session.h"

#include <QDateTime>
#include <QRegularExpression>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QtGlobal>

namespace eboks
{

Session::Session(QObject* parent) : QObject(parent), m_network(nullptr)
{
    m_network = new QNetworkAccessManager(this);
    connect(m_network, &QNetworkAccessManager::finished, this, &Session::networkReply);
}

QString Session::getUsername()
{
    return m_username;
}

std::shared_ptr<Request> Session::login(QString cpr, QString password, QString activation)
{
    m_cpr = cpr.toUtf8();
    m_password = password.toUtf8();
    m_activation = activation.toUtf8();

    auto request = std::make_shared<Request>(Request::Method::PUT, Request::Type::LOGIN, Url("session"));
    request->setContent(constructLoginXml().toUtf8(), "text/xml");
    enqueueRequest(request);
    return request;
}

std::shared_ptr<Request> Session::logout()
{
    auto request = std::make_shared<Request>(Request::Method::DELETE, Request::Type::LOGOUT, Url("session"));
    enqueueRequest(request);
    return request;
}

std::shared_ptr<Request> Session::getResource(Url url)
{
    auto request = std::make_shared<Request>(Request::Method::GET, Request::Type::QUERY, url);
    enqueueRequest(request);
    return request;
}

std::shared_ptr<Request> Session::putResource(Url url)
{
    return putResource(url, QByteArray(), "");
}

std::shared_ptr<Request> Session::putResource(Url url, const QByteArray& content, const QByteArray& contentType)
{
    auto request = std::make_shared<Request>(Request::Method::PUT, Request::Type::QUERY, url);
    request->setContent(content, contentType);
    enqueueRequest(request);
    return request;
}

std::shared_ptr<Request> Session::deleteResource(Url url)
{
    auto request = std::make_shared<Request>(Request::Method::DELETE, Request::Type::QUERY, url);
    enqueueRequest(request);
    return request;
}

void Session::fakeLogin(QString username, QString userid)
{
    m_activation = "REGEDE";
    m_cpr = "0123823";
    m_password = "jsdfine";
    m_sessionid = "1221456";
    m_nonce = "15443531";
    m_userid = userid;
    m_username = username;
}

void Session::setNetwork(QNetworkAccessManager* network)
{
    delete m_network;
    m_network = network;
    connect(m_network, &QNetworkAccessManager::finished, this, &Session::networkReply);
}

void Session::networkReply(QNetworkReply* reply)
{
    const auto request = m_pending.head();
    if (reply->error()) {
        emit error(reply, request);
    }

    switch (request->getType()) {
    case Request::Type::LOGIN:
        finishedRequestLogin(reply);
        break;
    case Request::Type::LOGOUT:
        finishedRequestLogout(reply);
        break;
    case Request::Type::QUERY:
        finishedRequestQuery(reply);
        break;
    }

    emit request->finished(reply);
    reply->deleteLater();
    dequeueRequest();
}

void Session::finishedRequestLogin(QNetworkReply* reply)
{
    if (!reply->error()) {
        extractSessionId(reply);
        extractNonce(reply);
        extractUserInfo(reply);
        emit loginFinished();
    }
}

void Session::finishedRequestLogout(QNetworkReply* reply)
{
    Q_UNUSED(reply);
    m_activation.clear();
    m_cpr.clear();
    m_password.clear();
    m_sessionid.clear();
    m_nonce.clear();
    m_userid.clear();
    m_username.clear();
    emit logoutFinished();
}

void Session::finishedRequestQuery(QNetworkReply* reply)
{
    extractNonce(reply);
}

bool Session::isLoggedIn() const
{
    return !m_sessionid.isEmpty();
}

QString Session::constructLoginXml() const
{
    QString text;
    QXmlStreamWriter xml(&text);

    xml.writeStartDocument();
    xml.writeStartElement("Logon");
    xml.writeAttribute("xmlns", "urn:eboks:mobile:1.0.0");

    xml.writeStartElement("User");
    xml.writeAttribute("identity", m_cpr);
    xml.writeAttribute("identityType", m_type);
    xml.writeAttribute("nationality", m_nationality);
    xml.writeAttribute("pincode", m_password);
    xml.writeEndElement();

    xml.writeEndElement();
    xml.writeEndDocument();

    return text;
}

void Session::extractSessionId(QNetworkReply* reply)
{
    QByteArray headername = "X-EBOKS-AUTHENTICATE";
    QByteArray headervalue = reply->rawHeader(headername);

    QRegularExpression regexp("sessionid=\"([0-9a-f\\-]*)\"");
    m_sessionid = regexp.match(headervalue).captured(1).toUtf8();

    if (m_sessionid.isEmpty()) {
        qFatal("missing sessionid in authentication header");
    }
}

void Session::extractNonce(QNetworkReply* reply)
{
    QByteArray authheadername = "X-EBOKS-AUTHENTICATE";

    // If there is no authentication header in the reply, the session is still
    // valid, but we have made a wrong api query. In this case we continue
    // with the current nonce value.
    if (!reply->hasRawHeader(authheadername)) {
        return;
    }

    QByteArray headervalue = reply->rawHeader(authheadername);
    QRegularExpression regexp("nonce=\"([0-9a-f]*)\"");
    m_nonce = regexp.match(headervalue).captured(1).toUtf8();

    if (m_nonce.isEmpty()) {
        qFatal("missing nonce in authentication header");
    }
}

void Session::extractUserInfo(QNetworkReply* reply)
{
    QXmlStreamReader xml(reply);
    m_userid.clear();
    m_username.clear();

    while (!xml.atEnd()) {
        xml.readNext();
        if (xml.isStartElement() && xml.name() == "User") {
            m_userid = xml.attributes().value("userId").toString();
            m_username = xml.attributes().value("name").toString();
            break;
        }
    }

    if (xml.error()) {
        qFatal("invalid logon xml");
    }

    if (m_userid.isEmpty()) {
        qFatal("missing userid in logon xml");
    }

    if (m_username.isEmpty()) {
        qFatal("missing username in logon xml");
    }
}

QByteArray Session::getAuthenticationLogin() const
{
    QDateTime datetime = QDateTime::currentDateTime();
    QByteArray datetimeText = datetime.toString(Qt::ISODate).toUtf8();
    QByteArray authentication =
        "deviceid=" + m_deviceid + ",datetime=" + datetimeText + ",challenge=" + getChallengeLogin(datetimeText);
    return authentication;
}

QByteArray Session::getAuthentication() const
{
    QByteArray authentication =
        "deviceid=" + m_deviceid + ",nonce=" + m_nonce + ",sessionid=" + m_sessionid + ",response=" + getChallenge();
    return authentication;
}

QByteArray Session::getChallengeLogin(const QByteArray& datetime) const
{
    QByteArray challenge = m_activation + ":" + m_deviceid + ":" + m_type + ":" + m_cpr + ":" + m_nationality + ":"
        + m_password + ":" + datetime;
    return doubleHashSha256Hex(challenge);
}

QByteArray Session::getChallenge() const
{
    QByteArray challenge = m_activation + ":" + m_deviceid + ":" + m_nonce + ":" + m_sessionid;
    return doubleHashSha256Hex(challenge);
}

QByteArray Session::doubleHashSha256Hex(const QByteArray& input) const
{
    QCryptographicHash single_hash(QCryptographicHash::Algorithm::Sha256);
    single_hash.addData(input);
    QCryptographicHash double_hash(QCryptographicHash::Algorithm::Sha256);
    double_hash.addData(single_hash.result().toHex());
    return double_hash.result().toHex();
}

void Session::enqueueRequest(std::shared_ptr<Request> request)
{
    m_pending.enqueue(request);
    if (m_pending.size() == 1) {
        sendRequest();
    }
}

void Session::dequeueRequest()
{
    m_pending.dequeue();

    if (!m_pending.isEmpty()) {
        sendRequest();
    }
}

void Session::sendRequest()
{
    const auto request = m_pending.head();

    // Setup the correct url.
    Url url = request->getUrl();
    if (request->getType() == Request::Type::QUERY) {
        url.setUserid(m_userid);
    }

    // Create the request.
    QNetworkRequest networkRequest(url.build());
    setupCommonHeaders(networkRequest);
    setupAuthenticationHeaders(networkRequest, request->getType());

    // Send the request. The networkReply function is called when the response
    // is received.
    switch (request->getMethod()) {
    case Request::Method::GET:
        m_network->get(networkRequest);
        break;
    case Request::Method::PUT:
        if (!request->getContentType().isEmpty()) {
            networkRequest.setRawHeader("Content-Type", request->getContentType());
        }
        m_network->put(networkRequest, request->getContent());
        break;
    case Request::Method::DELETE:
        m_network->deleteResource(networkRequest);
        break;
    }
}

void Session::setupCommonHeaders(QNetworkRequest& request) const
{
    QString useragent = QString("QtNetwork/") + qVersion();
    request.setRawHeader("User-Agent", useragent.toUtf8());
    request.setRawHeader("Host", "rest.e-boks.dk");
}

void Session::setupAuthenticationHeaders(QNetworkRequest& request, Request::Type type) const
{
    QByteArray authentication;
    switch (type) {
    case Request::Type::LOGIN:
        Q_ASSERT(!isLoggedIn());
        authentication = "logon " + getAuthenticationLogin();
        break;
    case Request::Type::LOGOUT:
        Q_ASSERT(isLoggedIn());
        authentication = "logoff " + getAuthentication();
        break;
    case Request::Type::QUERY:
        Q_ASSERT(isLoggedIn());
        authentication = getAuthentication();
        break;
    }
    request.setRawHeader("X-EBOKS-AUTHENTICATE", authentication);
}

} // namespace eboks
