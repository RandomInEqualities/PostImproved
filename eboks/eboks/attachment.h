#pragma once

#include "eboks/id.h"

#include <QString>

namespace eboks
{

// The Attachment class represent an eboks attachment. It holds the metadata
// of an attachment in eboks.
class Attachment
{
public:
    // The Data class is a thin wrapper around the metadata of an eboks
    // attachment.
    struct Data
    {
        QString title;
        AttachmentId id;
        int filesize;
        QString filetype;
    };

public:
    // Create attachment with invalid data.
    Attachment();

    // Create attachment with specified data.
    Attachment(const Data& data);

    // See if two attachments are the same. Compares the id's.
    bool operator==(const Attachment& attachment) const;
    bool operator!=(const Attachment& attachment) const;

    // Get the attachment title.
    QString getTitle() const;

    // Get the unique attachment identification number.
    AttachmentId getId() const;

    // Get the size in bytes of the attachment content.
    int getFileSize() const;

    // Get the type of attachment content (pdf, html, txt, etc.).
    QString getFileType() const;

private:
    Data m_data;
};

} // namespace eboks
