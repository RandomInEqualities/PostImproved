#include "eboks/downlad_manager.h"

#include "eboks/request.h"

#include <QDebug>
#include <QNetworkReply>
#include <QObject>
#include <QSet>
#include <QStack>

namespace eboks
{

DownloadManager::DownloadManager(eboks::Account* account)
    : QObject(account), m_account(account), m_downloadsInProgress(0)
{
}

void DownloadManager::downloadFolder(const eboks::Folder& folder, QDir directory)
{
    if (!directory.exists())
        return;

    auto request = m_account->getFolders();
    connect(request.get(), &eboks::Request::finished, this,
        [this, folder, directory](QNetworkReply* reply) { startFoldersDownload(reply, folder, directory); });
}

void DownloadManager::downloadMessage(const eboks::Message& message, QDir directory)
{
    if (!directory.exists())
        return;

    auto request = this->m_account->getMessageContent(message.getFolderId(), message.getId());
    connect(request.get(), &eboks::Request::finished, this,
        [this, message, directory](QNetworkReply* reply) { saveMessageContent(reply, message, directory); });
}

bool DownloadManager::isDownloading()
{
    return m_downloadsInProgress > 0;
}

void DownloadManager::saveMessageContent(QNetworkReply* reply, const eboks::Message& message, QDir directory)
{
    QString filename = findSuitableFileName(directory, message.getTitle(), message.getFileType());
    if (filename.isEmpty()) {
        qWarning() << "Unable to find a suitable filename:" << filename;
        return;
    }

    // Save the file.
    QString filepath = directory.absoluteFilePath(filename);
    QFile file(filepath);
    if (file.open(QFile::WriteOnly)) {
        file.write(reply->readAll());
    }
    else {
        qWarning() << "Could not write to:" << filepath;
    }
}

void DownloadManager::downloadFolder(QNetworkReply* reply, const Folder& folder, QDir directory, int skip)
{
    // Download the messages from the reply (if any).
    if (reply != nullptr) {
        if (reply->error()) {
            m_downloadsInProgress--;
            return;
        }

        auto messages = m_account->parseMessagesXml(reply);
        for (const auto& message : messages) {
            downloadMessage(message, directory);
        }

        // Check if we have found the last messages.
        if (messages.size() < TAKE_MESSAGES) {
            m_downloadsInProgress--;
            return;
        }
    }

    // Fetch metadata for the next messages.
    auto request = m_account->getFromFolder(folder.getId(), skip, TAKE_MESSAGES);
    connect(request.get(), &eboks::Request::finished, this, [this, folder, directory, skip](QNetworkReply* reply) {
        downloadFolder(reply, folder, directory, skip + TAKE_MESSAGES);
    });
}

void DownloadManager::startFoldersDownload(QNetworkReply* reply, const Folder& folder, QDir directory)
{
    if (reply->error())
        return;

    auto folders = m_account->parseFoldersXml(reply);

    // An e-Boks folder can contains several subfolders and these subfolders
    // can also have folders etc. We want to download everything and download
    // all the subfolders with a stack implementation.
    QStack<eboks::Folder> toDownload;
    QStack<QDir> toDirectory;

    toDownload.push(folder);
    toDirectory.push(directory);

    while (!toDownload.isEmpty()) {
        eboks::Folder outFolder = toDownload.pop();
        QDir outDirectory = toDirectory.pop();

        // Create or find the directory to put the folder messages in.
        bool found = findSuitableDirectory(outDirectory, outFolder.getName());
        if (!found) {
            qWarning() << "Unable to create directory named" << outFolder.getName() << "in" << outDirectory;
            continue;
        }

        // Put subfolders into the stack so they get downloaded.
        for (auto subfolderid : outFolder.getSubfolderIds()) {
            for (auto folder : folders) {
                if (folder.getId() == subfolderid) {
                    toDownload.push(folder);
                    toDirectory.push(outDirectory);
                    break;
                }
            }
        }

        // Download the folder.
        m_downloadsInProgress++;
        downloadFolder(nullptr, outFolder, outDirectory, 0);
    }
}

QString DownloadManager::findSuitableFileName(QDir directory, QString name, QString extension) const
{
    QString newName = escapeName(name);
    QString newExtension = escapeName(extension);
    QString filename = newName + "." + newExtension;

    int tries = 1;
    while (directory.exists(filename)) {
        filename = newName + "(" + QString::number(tries) + ")" + "." + newExtension;
        tries++;

        if (tries > MAX_FILENAME_TRIES) {
            return "";
        }
    }

    return filename;
}

bool DownloadManager::findSuitableDirectory(QDir& directory, QString name) const
{
    QString newName = escapeName(name);

    bool found = directory.cd(newName);
    if (found) {
        return true;
    }

    // Try to create the directory.
    directory.mkdir(newName);
    found = directory.cd(newName);

    if (found) {
        return true;
    }

    return false;
}

QString DownloadManager::escapeName(QString name) const
{
    // Note this implementation is rudimentatry and could be improved a lot.
    // Only tested on windows and linux.

    // Windows cannot use these character.
    QSet<QChar> disallow = {'/', '\\', ':', '*', '?', '"', '<', '>', '|'};
    QChar escapeWith = '_';

    for (auto& ch : name) {
        if (disallow.contains(ch)) {
            ch = escapeWith;
        }
    }

    return name;
}

} // namespace eboks
