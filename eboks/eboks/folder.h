#pragma once

#include "eboks/id.h"

#include <QString>
#include <QVector>

namespace eboks
{

// The Folder class represent an eboks folder. It holds the metadata of a
// folder in eboks.
class Folder
{
public:
    // The Type enum describes the different types of eboks folders.
    enum class Type
    {
        // Folder where all mail is placed by default. This type is "B" in
        // eboks api responses.
        Inbox,

        // Folder for mail that have not yet been sent. This type is "K" in
        // eboks api responses.
        Drafts,

        // Folder for deleted mail. This type is "P" in eboks api responses.
        // Mail inhere will be permanently delete after 30 days.
        DeletedItems,

        // Sent items folder for mail that you have sent. This type is "S" in
        // eboks api responses.
        SentItems,

        // Custom folder that user have created. This type is "O" is eboks api
        // responses.
        CustomFolder,

        // Custom subfolder that user have created. This type is "U" in eboks
        // api responses.
        CustomSubfolder,

        // Folder that contain all top-level folders. It does not actually
        // exists in eboks.
        RootFolder
    };

    // Conversion between strings and folder types. Will warn you if conversion
    // could not be performed.
    static Type typeFromString(QString string);

public:
    // The Data class is a thin wrapper around the metadata of an eboks folder.
    struct Data
    {
        QString name;
        FolderId id;
        int unreadAmount;
        Type type;
        FolderId parentId;
        QVector<FolderId> subfolderIds;
    };

public:
    // Create folder with invalid data.
    Folder();

    // Create folder with specified data.
    Folder(const Data& data);

    // See if two folders are the same. Compares the id's.
    bool operator==(const Folder& folder) const;
    bool operator!=(const Folder& folder) const;

    // Get the folder name.
    QString getName() const;

    // Get the unique folder identification number.
    FolderId getId() const;

    // Get the amount of unread messages in the folder.
    int getUnreadAmount() const;

    // Get the folder type.
    Type getType() const;

    // Is the folder a customer folder or custom subfolder. This is a
    // convienince method to check if the folder was created by the user.
    bool isCustomFolder() const;

    // Get the parent folder id. The id is "0" if this folder is a toplevel
    // folder.
    FolderId getParentId() const;

    // Get the ids for the folders inside this folder.
    QVector<FolderId> getSubfolderIds() const;

private:
    Data m_data;
};

} // namespace eboks
