#include "eboks/attachment.h"

namespace eboks
{

Attachment::Attachment() : m_data()
{
}

Attachment::Attachment(const Attachment::Data& data) : m_data(data)
{
}

bool Attachment::operator==(const Attachment& attachment) const
{
    return m_data.id == attachment.m_data.id;
}

bool Attachment::operator!=(const Attachment& attachment) const
{
    return !operator==(attachment);
}

QString Attachment::getTitle() const
{
    return m_data.title;
}

AttachmentId Attachment::getId() const
{
    return m_data.id;
}

int Attachment::getFileSize() const
{
    return m_data.filesize;
}

QString Attachment::getFileType() const
{
    return m_data.filetype;
}

} // namespace eboks
