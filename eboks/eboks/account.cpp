#include "eboks/account.h"

#include <QDateTime>
#include <QFile>
#include <QString>
#include <QTextStream>

namespace eboks
{

Account::Account(Session* session) : QObject(session), m_session(session)
{
}

std::shared_ptr<Request> Account::getFolders()
{
    return m_session->getResource(Url("0/mail/folders"));
}

QVector<Folder> Account::parseFoldersXml(QNetworkReply* reply) const
{
    QXmlStreamReader xml(reply);
    QVector<Folder> folders;

    while (!xml.atEnd()) {
        xml.readNext();

        if (xml.isStartElement() && xml.name() == "FolderInfo") {
            readFolderInfo(xml, FolderId("0"), folders);
        }
    }

    return folders;
}

Folder Account::readFolderInfo(QXmlStreamReader& xml, FolderId parentId, QVector<Folder>& folders) const
{
    Q_ASSERT(xml.isStartElement());
    Q_ASSERT(xml.name() == "FolderInfo");

    Folder::Data data;
    data.parentId = parentId;

    Q_ASSERT(xml.attributes().hasAttribute("name"));
    data.name = xml.attributes().value("name").toString();

    Q_ASSERT(xml.attributes().hasAttribute("id"));
    data.id = FolderId(xml.attributes().value("id").toString());

    Q_ASSERT(xml.attributes().hasAttribute("unread"));
    data.unreadAmount = xml.attributes().value("unread").toInt();

    Q_ASSERT(xml.attributes().hasAttribute("type"));
    data.type = Folder::typeFromString(xml.attributes().value("type").toString());

    // Find subfolders.
    while (!xml.atEnd()) {
        xml.readNext();

        if (xml.isEndElement() && xml.name() == "FolderInfo") {
            break;
        }
        else if (xml.isStartElement() && xml.name() == "FolderInfo") {
            Folder child = readFolderInfo(xml, data.id, folders);
            data.subfolderIds.append(child.getId());
        }
    }

    folders.append(Folder(data));

    return folders.back();
}

std::shared_ptr<Request> Account::createFolder(QString name, FolderId parentId)
{
    Url url = getFolderUrl(parentId);
    url.addQuery("folderName", name);
    auto request = m_session->putResource(url);

    // Emit a signal if the request was a success.
    auto notify = [this, parentId](QNetworkReply* reply) {
        if (!reply->error()) {
            emit folderCreated(parentId);
        }
    };
    connect(request.get(), &eboks::Request::finished, this, notify);
    return request;
}

std::shared_ptr<Request> Account::deleteFolder(FolderId folderId)
{
    Url url = getFolderUrl(folderId);
    auto request = m_session->deleteResource(url);

    // Emit a signal if the request was a success.
    auto notify = [this, folderId](QNetworkReply* reply) {
        if (!reply->error()) {
            emit folderDeleted(folderId);
        }
    };
    connect(request.get(), &eboks::Request::finished, this, notify);
    return request;
}

std::shared_ptr<Request> Account::updateFolder(FolderId folderId, FolderId newParentId, QString newName)
{
    Url url = getFolderUrl(folderId);
    url.addQuery("toFolderId", newParentId.toString());
    url.addQuery("newFolderName", newName);

    auto request = m_session->putResource(url);

    // Emit a signal if the request was a success.
    auto notify = [this, folderId, newParentId, newName](QNetworkReply* reply) {
        if (!reply->error()) {
            emit folderUpdated(folderId, newParentId.toString(), newName);
        }
    };
    connect(request.get(), &eboks::Request::finished, this, notify);
    return request;
}

std::shared_ptr<Request> Account::getSenders()
{
    return m_session->getResource(Url("0/mail/folders/search/senders"));
}

QVector<Sender> Account::parseSendersXml(QNetworkReply* reply) const
{
    QXmlStreamReader xml(reply);
    QVector<Sender> senders;

    while (!xml.atEnd()) {
        xml.readNext();

        if (xml.isStartElement() && xml.name() == "Sender") {
            senders.append(readSender(xml));
        }
    }

    if (xml.error()) {
        qFatal("eboks::Account: parse sender failure");
    }

    return senders;
}

Sender Account::readSender(QXmlStreamReader& xml) const
{
    Q_ASSERT(xml.isStartElement());
    Q_ASSERT(xml.name() == "Sender");

    SenderId id;
    QString name;

    if (xml.attributes().hasAttribute("id")) {
        id = SenderId(xml.attributes().value("id").toString());
        name = xml.readElementText();
    }
    else {
        // Uploaded by the user. Give him sender id 0.
        id = SenderId("0");
        name = m_session->getUsername();
    }

    return Sender(name, id);
}

std::shared_ptr<Request> Account::getLatest()
{
    return m_session->getResource(Url("0/mail/folder/search/latest"));
}

std::shared_ptr<Request> Account::getUnread(int skip, int take)
{
    Q_ASSERT(skip >= 0 && take >= 0);
    Url url("0/mail/folder/search/unread");
    url.addQuery("skip", skip);
    url.addQuery("take", take);
    return m_session->getResource(url);
}

std::shared_ptr<Request> Account::getBetweenDates(QDateTime start, QDateTime end)
{
    Q_ASSERT(start.isValid());
    Q_ASSERT(end.isValid());
    Url url("0/mail/folder/search/latest");
    url.addQuery("from", start.toString(Qt::ISODate));
    url.addQuery("to", end.toString(Qt::ISODate));
    return m_session->getResource(url);
}

std::shared_ptr<Request> Account::getFromFolder(FolderId folderId, int skip, int take)
{
    Q_ASSERT(skip >= 0 && take >= 0);
    Url url = getFolderUrl(folderId);
    url.addQuery("skip", skip);
    url.addQuery("take", take);
    return m_session->getResource(url);
}

std::shared_ptr<Request> Account::getFromSender(SenderId senderId, int skip, int take)
{
    Q_ASSERT(skip >= 0 && take >= 0);
    Url url("0/mail/folder/search/senders/" + senderId.toString());
    url.addQuery("skip", skip);
    url.addQuery("take", take);
    return m_session->getResource(url);
}

std::shared_ptr<Request> Account::getFromSearch(QString search, int skip, int take)
{
    Q_ASSERT(skip >= 0 && take >= 0);
    Url url("0/mail/folder/search");
    url.addQuery("search", search);
    url.addQuery("skip", skip);
    url.addQuery("take", take);
    return m_session->getResource(url);
}

QVector<Message> Account::parseMessagesXml(QNetworkReply* reply) const
{
    QXmlStreamReader xml(reply);
    QVector<Message> messages;

    while (!xml.atEnd()) {
        xml.readNext();

        QString n = xml.name().toString();

        if (xml.isStartElement() && xml.name() == "MessageInfo") {
            messages.append(readMessageInfo(xml));
        }
    }

    if (xml.error()) {
        qFatal("eboks::Account: parse messages failure");
    }

    return messages;
}

Message Account::readMessageInfo(QXmlStreamReader& xml) const
{
    Q_ASSERT(xml.isStartElement());
    Q_ASSERT(xml.name() == "MessageInfo");

    Message::Data data;

    Q_ASSERT(xml.attributes().hasAttribute("name"));
    data.title = xml.attributes().value("name").toString();

    Q_ASSERT(xml.attributes().hasAttribute("id"));
    data.id = MessageId(xml.attributes().value("id").toString());

    Q_ASSERT(xml.attributes().hasAttribute("receivedDateTime"));
    data.receiveTime = QDateTime::fromString(xml.attributes().value("receivedDateTime").toString(), Qt::ISODate);

    Q_ASSERT(xml.attributes().hasAttribute("folderId"));
    data.folderId = FolderId(xml.attributes().value("folderId").toString());

    Q_ASSERT(xml.attributes().hasAttribute("unread"));
    data.isUnread = stringToBool(xml.attributes().value("unread").toString());

    Q_ASSERT(xml.attributes().hasAttribute("size"));
    data.filesize = xml.attributes().value("size").toInt();

    Q_ASSERT(xml.attributes().hasAttribute("format"));
    data.filetype = xml.attributes().value("format").toString().toLower();

    Q_ASSERT(xml.attributes().hasAttribute("attachmentsCount"));
    data.attachmentCount = xml.attributes().value("attachmentsCount").toInt();

    while (!xml.atEnd()) {
        xml.readNext();

        if (xml.isEndElement() && xml.name() == "MessageInfo") {
            break;
        }
        else if (xml.isStartElement() && xml.name() == "Note") {
            data.note = xml.readElementText();
        }
        else if (xml.isStartElement() && xml.name() == "Sender") {
            data.sender = readSender(xml);
        }
    }

    Q_ASSERT(!data.sender.getId().toString().isEmpty());
    Q_ASSERT(!data.sender.getName().isEmpty());

    return Message(data);
}

std::shared_ptr<Request> Account::getMessageContent(FolderId folderId, MessageId messageId)
{
    Url url("0/mail/folder/" + folderId.toString() + "/message/" + messageId.toString() + "/content");
    return m_session->getResource(url);
}

std::shared_ptr<Request> Account::moveMessage(FolderId folderId, MessageId messageId, FolderId newFolderId)
{
    Q_ASSERT(folderId != newFolderId);

    Url url = getMessageUrl(folderId, messageId);
    url.addQuery("toFolderId", newFolderId.toString());
    auto request = m_session->putResource(url);

    // Emit a signal if the request was a success.
    auto notify = [this, folderId, messageId, newFolderId](QNetworkReply* reply) {
        if (!reply->error()) {
            emit messageMoved(folderId, messageId, newFolderId);
        }
    };
    connect(request.get(), &eboks::Request::finished, this, notify);
    return request;
}

std::shared_ptr<Request> Account::deleteMessage(FolderId folderId, MessageId messageId)
{
    Url url = getMessageUrl(folderId, messageId);
    auto request = m_session->deleteResource(url);
    emit messageDeleted(folderId, messageId);

    // Emit a signal if the request was a success.
    auto notify = [this, folderId, messageId](QNetworkReply* reply) {
        if (!reply->error()) {
            emit messageDeleted(folderId, messageId);
        }
    };
    connect(request.get(), &eboks::Request::finished, this, notify);
    return request;
}

std::shared_ptr<Request> Account::setMessageTitle(FolderId folderId, MessageId messageId, QString newTitle)
{
    Url url = getMessageUrl(folderId, messageId);
    url.addQuery("newName", newTitle);

    auto request = m_session->putResource(url);

    // Emit a signal if the request was a success.
    auto notify = [this, folderId, messageId, newTitle](QNetworkReply* reply) {
        if (!reply->error()) {
            MessageUpdateInfo info;
            info.newTitle = newTitle;
            emit messageUpdated(folderId, messageId, info);
        }
    };
    connect(request.get(), &eboks::Request::finished, this, notify);

    return request;
}

std::shared_ptr<Request> Account::setMessageNote(FolderId folderId, MessageId messageId, QString newNote)
{
    Url url = getMessageUrl(folderId, messageId);
    url.addQuery("note", newNote);
    auto request = m_session->putResource(url);

    // Emit a signal if the request was a success.
    auto notify = [this, folderId, messageId, newNote](QNetworkReply* reply) {
        if (!reply->error()) {
            MessageUpdateInfo info;
            info.newNote = newNote;
            emit messageUpdated(folderId, messageId, info);
        }
    };
    connect(request.get(), &eboks::Request::finished, this, notify);
    return request;
}

std::shared_ptr<Request> Account::setMessageRead(FolderId folderId, MessageId messageId, bool newIsRead)
{
    Url url = getMessageUrl(folderId, messageId);
    url.addQuery("read", boolToString(newIsRead));
    auto request = m_session->putResource(url);

    // Emit a signal if the request was a success.
    auto notify = [this, folderId, messageId, newIsRead](QNetworkReply* reply) {
        if (!reply->error()) {
            MessageUpdateInfo info;
            info.newIsUnread = !newIsRead;
            emit messageUpdated(folderId, messageId, info);
        }
    };
    connect(request.get(), &eboks::Request::finished, this, notify);
    return request;
}

std::shared_ptr<Request> Account::getAttachments(FolderId folderId, MessageId messageId)
{
    Url url = getMessageUrl(folderId, messageId);
    return m_session->getResource(url);
}

QVector<Attachment> Account::parseAttachmentsXml(QNetworkReply* reply) const
{
    QXmlStreamReader xml(reply);
    QVector<Attachment> attachments;

    while (!xml.atEnd()) {
        xml.readNext();

        if (xml.isStartElement() && xml.name() == "AttachmentInfo") {
            attachments.append(readAttachmentInfo(xml));
        }
    }

    if (xml.error()) {
        qFatal("eboks::Account: parse attachments failure");
    }

    return attachments;
}

Attachment Account::readAttachmentInfo(QXmlStreamReader& xml) const
{
    Q_ASSERT(xml.isStartElement());
    Q_ASSERT(xml.name() == "AttachmentInfo");

    Attachment::Data data;

    Q_ASSERT(xml.attributes().hasAttribute("name"));
    data.title = xml.attributes().value("name").toString();

    Q_ASSERT(xml.attributes().hasAttribute("id"));
    data.id = AttachmentId(xml.attributes().value("id").toString());

    Q_ASSERT(xml.attributes().hasAttribute("size"));
    data.filesize = xml.attributes().value("size").toInt();

    Q_ASSERT(xml.attributes().hasAttribute("format"));
    data.filetype = xml.attributes().value("format").toString();

    return Attachment(data);
}

std::shared_ptr<Request> Account::getAttachmentContent(FolderId folderId, AttachmentId attachmentId)
{
    Url url("0/mail/folder/" + folderId.toString() + "/message/" + attachmentId.toString() + "/content");
    return m_session->getResource(url);
}

std::shared_ptr<Request> Account::uploadMessage(FolderId toFolderId, QString name, QString content, QString filetype)
{
    return uploadBytes(toFolderId, name, content.toUtf8(), filetype);
}

std::shared_ptr<Request> Account::uploadFile(FolderId toFolderId, QString name, QString filename, QString filetype)
{
    QFile file(filename);
    file.open(QFile::ReadOnly);
    if (!file.isOpen()) {
        qFatal("eboks::Account: unable to open file");
    }
    return uploadBytes(toFolderId, name, file.readAll(), filetype);
}

std::shared_ptr<Request> Account::uploadBytes(FolderId toFolderId, QString name, QByteArray content, QString filetype)
{
    Url url = getFolderUrl(toFolderId);
    url.addQuery("name", name);
    url.addQuery("filetype", filetype);
    auto request = m_session->putResource(url, content, "application/octet");

    // Emit a signal if the request was a success.
    auto notify = [this, toFolderId](QNetworkReply* reply) {
        if (!reply->error()) {
            emit messageCreated(toFolderId);
        }
    };
    connect(request.get(), &eboks::Request::finished, this, notify);
    return request;
}

Url Account::getFolderUrl(FolderId folderId) const
{
    return Url("0/mail/folder/" + folderId.toString());
}

Url Account::getMessageUrl(FolderId folderId, MessageId messageId) const
{
    return Url("0/mail/folder/" + folderId.toString() + "/message/" + messageId.toString());
}

bool Account::stringToBool(QString string) const
{
    if (string == "true") {
        return true;
    }
    else if (string == "false") {
        return false;
    }
    else {
        qWarning("eboks::Account: invalid string to bool conversion");
        return false;
    }
}

QString Account::boolToString(bool boolean) const
{
    return (boolean) ? "true" : "false";
}

} // namespace eboks
