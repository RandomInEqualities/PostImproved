#include "folder_tree_model.h"

#include "eboks/id.h"
#include "eboks/request.h"

#include <QMessageBox>

FolderTreeItem::FolderTreeItem(eboks::Folder folder, FolderTreeItem* parentItem)
    : m_folder(folder), m_parent(parentItem)
{
}

FolderTreeItem::~FolderTreeItem()
{
    qDeleteAll(m_children);
}

FolderTreeItem* FolderTreeItem::getChild(int index)
{
    return m_children.at(index);
}

int FolderTreeItem::getChildCount() const
{
    return m_children.count();
}

int FolderTreeItem::getIndex() const
{
    if (m_parent) {
        return m_parent->m_children.indexOf(const_cast<FolderTreeItem*>(this));
    }
    return 0;
}

FolderTreeItem* FolderTreeItem::getParent()
{
    return m_parent;
}

void FolderTreeItem::appendChild(FolderTreeItem* folder)
{
    m_children.append(folder);
}

const eboks::Folder& FolderTreeItem::getFolder() const
{
    return m_folder;
}

FolderTreeModel::FolderTreeModel(QObject* parent, eboks::Account* account)
    : QAbstractItemModel(parent), m_rootItem(nullptr), m_account(account)
{
    connect(account, &eboks::Account::folderCreated, this, [=](eboks::FolderId) { refreshTree(); });
    connect(account, &eboks::Account::folderDeleted, this, [=](eboks::FolderId) { refreshTree(); });
    connect(account, &eboks::Account::folderUpdated, this, [=](eboks::FolderId, QVariant, QVariant) { refreshTree(); });

    refreshTree();
}

FolderTreeModel::~FolderTreeModel()
{
    delete m_rootItem;
    m_rootItem = nullptr;
}

QVariant FolderTreeModel::data(const QModelIndex& index, int role) const
{
    if (m_rootItem == nullptr) {
        return QVariant();
    }
    if (!index.isValid()) {
        return QVariant();
    }
    if (role != Qt::DisplayRole && role != Qt::EditRole) {
        return QVariant();
    }
    if (index.column() != 0) {
        return QVariant();
    }
    if (role == Qt::EditRole) {
        return this->getFolder(index).getName();
    }
    FolderTreeItem* item = getFolderItem(index);
    return item->getFolder().getName();
}

bool FolderTreeModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (!index.isValid()) {
        return false;
    }
    if (role != Qt::EditRole) {
        return false;
    }
    if (index.column() != 0) {
        return false;
    }

    eboks::Folder folder = getFolderItem(index)->getFolder();
    QString newName = value.toString();

    if (!newName.isEmpty()) {
        if (!folder.isCustomFolder()) {
            QMessageBox msgBox;
            msgBox.setText("This is a default e-Boks folder that can't be renamed.");
            msgBox.exec();
            return false;
        }
        m_account->updateFolder(folder.getId(), folder.getParentId(), newName);
        return true;
    }

    return false;
}

bool FolderTreeModel::dropMimeData(
    const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent)
{
    Q_UNUSED(row);
    Q_UNUSED(column);

    if (action != Qt::MoveAction) {
        return false;
    }
    if (!data->hasFormat("text/xml")) {
        return false;
    }

    eboks::Folder targetFolder = getFolderItem(parent)->getFolder();
    QXmlStreamReader xml(data->data("text/xml"));

    while (!xml.atEnd()) {
        xml.readNext();
        if (xml.isStartElement() && xml.name() == "Message") {
            // A dropped message.

            // We can't drop into the root folder.
            if (targetFolder.getId() == ROOT_FOLDER_ID) {
                continue;
            }

            QString id = xml.attributes().value("id").toString();
            QString folderId = xml.attributes().value("folderId").toString();
            m_account->moveMessage(folderId, id, targetFolder.getId());
        }
        else if (xml.isStartElement() && xml.name() == "Folder") {
            // A dropped Folder.
            QString id = xml.attributes().value("id").toString();
            QString name = xml.attributes().value("name").toString();
            m_account->updateFolder(id, targetFolder.getId(), name);
        }
    }

    Q_ASSERT(!xml.error());
    return true;
}

Qt::DropActions FolderTreeModel::supportedDragActions() const
{
    return Qt::MoveAction;
}

Qt::DropActions FolderTreeModel::supportedDropActions() const
{
    return Qt::MoveAction;
}

Qt::ItemFlags FolderTreeModel::flags(const QModelIndex& index) const
{
    // If the index is invalid it might be the root folder. We want to drop
    // stuff into this.
    if (!index.isValid()) {
        return Qt::ItemIsDropEnabled;
    }

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsDragEnabled
        | Qt::ItemIsDropEnabled;
}

QStringList FolderTreeModel::mimeTypes() const
{
    return {"text/xml"};
}

QMimeData* FolderTreeModel::mimeData(const QModelIndexList& indices) const
{
    QMimeData* mimeData = new QMimeData();

    QByteArray text;
    QXmlStreamWriter writer(&text);

    writer.writeStartElement("Folders");
    for (auto index : indices) {
        Q_ASSERT(index.isValid());
        eboks::Folder folder = getFolderItem(index)->getFolder();
        writer.writeStartElement("Folder");
        writer.writeAttribute("id", folder.getId().toString());
        writer.writeAttribute("name", folder.getName());
        writer.writeEndElement();
    }
    writer.writeEndElement();

    mimeData->setData("text/xml", text);
    return mimeData;
}

int FolderTreeModel::rowCount(const QModelIndex& parent) const
{
    if (m_rootItem == nullptr) {
        return 0;
    }
    FolderTreeItem* parentItem = getFolderItem(parent);
    return parentItem->getChildCount();
}

int FolderTreeModel::columnCount(const QModelIndex& /*parent*/) const
{
    // Right now we only have 1 column: the folder name.
    return 1;
}

QModelIndex FolderTreeModel::index(int row, int column, const QModelIndex& parent) const
{
    if (m_rootItem == nullptr) {
        return QModelIndex();
    }
    if (!hasIndex(row, column, parent)) {
        return QModelIndex();
    }

    FolderTreeItem* parentItem = getFolderItem(parent);
    FolderTreeItem* childItem = parentItem->getChild(row);
    return createIndex(row, column, childItem);
}

QModelIndex FolderTreeModel::parent(const QModelIndex& index) const
{
    if (m_rootItem == nullptr) {
        return QModelIndex();
    }
    if (!index.isValid()) {
        return QModelIndex();
    }

    FolderTreeItem* childItem = getFolderItem(index);
    FolderTreeItem* parentItem = childItem->getParent();

    if (parentItem == m_rootItem) {
        return QModelIndex();
    }
    return createIndex(parentItem->getIndex(), index.column(), parentItem);
}

eboks::Folder FolderTreeModel::getFolder(const QModelIndex& index) const
{
    Q_ASSERT(index.isValid());
    return getFolderItem(index)->getFolder();
}

FolderTreeItem* FolderTreeModel::getFolderItem(const QModelIndex& index) const
{
    if (index.isValid()) {
        return static_cast<FolderTreeItem*>(index.internalPointer());
    }
    return m_rootItem;
}

void FolderTreeModel::refreshTree()
{
    auto request = m_account->getFolders();
    connect(request.get(), &eboks::Request::finished, this, &FolderTreeModel::buildFolderTree);
}

void FolderTreeModel::buildFolderTree(QNetworkReply* reply)
{
    if (reply->error()) {
        return;
    }

    beginResetModel();
    delete m_rootItem;

    // Create the root folder item.
    eboks::Folder::Data data;
    data.parentId = eboks::FolderId("");
    data.name = "root";
    data.id = ROOT_FOLDER_ID;
    data.unreadAmount = 0;
    data.type = eboks::Folder::Type::RootFolder;
    eboks::Folder rootFolder(data);

    m_rootItem = new FolderTreeItem(rootFolder, nullptr);

    // Create the subfolder items from the reply and insert them in the root.
    QVector<eboks::Folder> folders = m_account->parseFoldersXml(reply);
    buildSubfolderItems(m_rootItem, folders);

    endResetModel();
}

void FolderTreeModel::buildSubfolderItems(FolderTreeItem* item, const QVector<eboks::Folder>& folders)
{
    for (const auto& folder : folders) {
        if (folder.getParentId() == item->getFolder().getId()) {
            auto childItem = new FolderTreeItem(folder, item);
            buildSubfolderItems(childItem, folders);
            item->appendChild(childItem);
        }
    }
}
