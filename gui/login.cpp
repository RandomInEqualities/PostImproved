#include "login.h"

#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <QSettings>

Login::Login(AppController* app, eboks::Session* session)
    : QWidget(app),
      m_app(app),
      m_session(session),
      m_cprValidator(nullptr),
      m_passwordValidator(nullptr),
      m_activationValidator(nullptr)
{
    setupUi(this);

    m_cprValidator = constructValidator(cprInput, "[0-9]{6}\\-?[0-9]{4}");
    m_passwordValidator = constructValidator(passwordInput, ".{6,}");
    m_activationValidator = constructValidator(activationInput, "[0-9a-zA-Z]{8}");

    loadLoginCredentials();
    initializeFocus();
    on_loginButton_clicked();
}

void Login::loginReply(QNetworkReply* reply)
{
    if (!reply->error()) {
        errorLabel->clear();

        if (rememberCheckBox->isChecked()) {
            saveLoginCredentials();
        }

        m_app->showMenu();
    }
    else {
        if (reply->error() == 302) {
            // Wrong cpr, password or activation code.
            errorLabel->setText("Wrong cpr, password or activation code.");
        }
        else if (reply->error() < 200) {
            // Something wrong with the network connection.
            errorLabel->setText("Network connection could not be established.");
        }
        else {
            // Something wrong with the e-Boks servers.
            errorLabel->setText("Critical login error.");
        }
    }

    loginButton->setEnabled(true);
}

void Login::on_loginButton_clicked()
{
    QString cpr = cprInput->text();
    QString password = passwordInput->text();
    QString activation = activationInput->text();

    if (!isValidInput(cpr, m_cprValidator)) {
        return;
    }
    if (!isValidInput(password, m_passwordValidator)) {
        return;
    }
    if (!isValidInput(activation, m_activationValidator)) {
        return;
    }

    auto request = m_session->login(cpr, password, activation);
    connect(request.get(), &eboks::Request::finished, this, &Login::loginReply);

    loginButton->setEnabled(false);
}

QValidator* Login::constructValidator(QLineEdit* widget, QString regexp)
{
    auto validator = new QRegularExpressionValidator(this);
    validator->setRegularExpression(QRegularExpression(regexp));
    widget->setValidator(validator);

    // Update the border color on each character input. This shows a red border
    // as long as the input is clearly wrong. Uses a CSS property to style the
    // border in the UI form file.
    connect(widget, &QLineEdit::textChanged, this, [this, widget, validator](const QString& text) {
        widget->setProperty("valid", isValidInput(text, validator) || text.isEmpty());
        widget->style()->unpolish(widget);
        widget->style()->polish(widget);
        widget->update();
    });

    return validator;
}

bool Login::isValidInput(QString input, QValidator* validator) const
{
    int position = 0;
    return validator->validate(input, position) == QValidator::Acceptable;
}

void Login::saveLoginCredentials() const
{
    QSettings settings;
    settings.setValue("login/cpr", cprInput->text());
    settings.setValue("login/activation", activationInput->text());
}

void Login::loadLoginCredentials() const
{
    QSettings settings;
    if (settings.contains("login/cpr")) {
        cprInput->setText(settings.value("login/cpr").toString());
    }
    if (settings.contains("login/password")) {
        passwordInput->setText(settings.value("login/password").toString());
    }
    if (settings.contains("login/activation")) {
        activationInput->setText(settings.value("login/activation").toString());
    }
}

void Login::initializeFocus()
{
    if (cprInput->text().isEmpty()) {
        cprInput->setFocus();
    }
    else if (passwordInput->text().isEmpty()) {
        passwordInput->setFocus();
    }
    else if (activationInput->text().isEmpty()) {
        activationInput->setFocus();
    }
    else {
        cprInput->setFocus();
    }
}
