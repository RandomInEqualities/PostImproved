#pragma once

#include "eboks/message.h"

#include <QByteArray>
#include <QMap>
#include <QQueue>
#include <QScopedPointer>
#include <QWebEngineDownloadItem>
#include <QWebEnginePage>
#include <QWebEngineProfile>
#include <QWebEngineProfile>
#include <QWebEngineView>
#include <QWidget>

class MessageContentPage : public QWebEnginePage
{
private:
    Q_OBJECT

public:
    MessageContentPage(QWebEngineProfile* profile);

private slots:

    // Don't allow webpage extra features, like fullscreen, location data, etc.
    void featurePermissionRequestHandle(const QUrl& securityOrigin, QWebEnginePage::Feature feature);
};

// Widget that represents a browser page. Renders message content.
class MessageContent : public QWebEngineView
{
private:
    Q_OBJECT

public:
    MessageContent(QWidget* parent);
    ~MessageContent();

    // Render and show the message content.
    void render(const eboks::Message& message, const QByteArray& content);

    // Prepare showing the message content by rendering it offscreen. A
    // subsequent render call will then be much faster.
    void preRender(const eboks::Message& message, const QByteArray& content);

private slots:

    void downloadRequested(QWebEngineDownloadItem* item);

private:
    // Disable context menu, we don't use at the moment.
    void contextMenuEvent(QContextMenuEvent* event);

    QWebEnginePage* renderPdf(const eboks::Message& message, const QByteArray& content);
    QWebEnginePage* renderHtml(const eboks::Message& message, const QByteArray& content);
    QWebEnginePage* renderText(const eboks::Message& message, const QByteArray& content);
    QWebEnginePage* renderImage(const eboks::Message& message, const QByteArray& content);
    QWebEnginePage* renderError(const eboks::Message& message, const QByteArray& content);

    // Profile that is shared among all pages.
    QWebEngineProfile* const m_profile = nullptr;
};
