#pragma once

#include "eboks/account.h"

#include <QMouseEvent>
#include <QtCore>
#include <QtNetwork>

// Item objects for use in the FolderTreeModel. Is a small wrapper around the
// data of an eboks folder. Makes it easier to show a hierarchical folder tree.
// It has a parent item and sub items, that all represents the parent folder
// and the subfolders to this item.
class FolderTreeItem
{
public:
    FolderTreeItem(eboks::Folder folder, FolderTreeItem* parentItem);
    ~FolderTreeItem();

    FolderTreeItem* getChild(int index);
    int getChildCount() const;
    int getIndex() const;
    FolderTreeItem* getParent();
    void appendChild(FolderTreeItem* child);

    const eboks::Folder& getFolder() const;

private:
    eboks::Folder m_folder;
    FolderTreeItem* m_parent;
    QList<FolderTreeItem*> m_children;
};

// Model to represent the eboks folder hierarchy is a QAbstractItemModel, such
// that it fits into the QT modelview architechture and is easy to display.
class FolderTreeModel : public QAbstractItemModel
{
private:
    Q_OBJECT

public:
    FolderTreeModel(QObject* parent, eboks::Account* account);
    ~FolderTreeModel();

    // Get the data in the model.
    QVariant data(const QModelIndex& index, int role) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Set the data, used for changing a folders name.
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

    // Used for drag and drop support. It support dragging both folders and
    // messages into other folders.
    bool dropMimeData(
        const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent) override;
    Qt::DropActions supportedDragActions() const override;
    Qt::DropActions supportedDropActions() const override;
    QStringList mimeTypes() const override;
    QMimeData* mimeData(const QModelIndexList& indexes) const override;

    // Methods for construct the model indices.
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex& index) const override;

    // Get the folder folder that an index points to. The index must be valid
    // before calling. Otherwise the root folder is returned.
    eboks::Folder getFolder(const QModelIndex& index) const;

    // Get the folder item that an index points to.
    FolderTreeItem* getFolderItem(const QModelIndex& index) const;

private slots:

    // Send a request to the eboks servers to get the tree layout.
    void refreshTree();

    // Rebuild the tree layout from an eboks reponse.
    void buildFolderTree(QNetworkReply* reply);

private:
    // Recursively find and construct subfolder items to an item.
    void buildSubfolderItems(FolderTreeItem* item, const QVector<eboks::Folder>& folders);

    const eboks::FolderId ROOT_FOLDER_ID = eboks::FolderId("0");

    FolderTreeItem* m_rootItem;
    eboks::Account* m_account;
};
