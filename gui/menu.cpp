#include "menu.h"

#include "eboks/downlad_manager.h"

#include <QMessageBox>

Menu::Menu(AppController* app, eboks::Account* account) : QWidget(app), m_app(app), m_account(account)
{
    setupUi(this);
}

void Menu::on_mailboxButton_clicked()
{
    m_app->showMailBox();
}

void Menu::on_tilemenuButton_clicked()
{
    m_app->showTileMenu();
}

void Menu::on_logoutButton_clicked()
{
    m_app->showLogin();
}

void Menu::on_searchButton_clicked()
{
    m_app->showSearch();
}
