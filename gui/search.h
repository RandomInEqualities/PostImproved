#pragma once

#include "ui_search.h"

#include "app_controller.h"

#include "eboks/account.h"

#include <QWidget>

class Search : public QWidget, private Ui::Search
{
private:
    Q_OBJECT

public:
    Search(AppController* parent);

private slots:

    void on_backButton_clicked();
    void on_searchButton_clicked();

private:
    AppController* m_app;
};
