#include "search.h"

#include "filter_model.h"

Search::Search(AppController* parent) : QWidget(parent), m_app(parent)
{
    setupUi(this);
    startDateInput->setDate(QDate::currentDate().addDays(-1));
    endDateInput->setDate(QDate::currentDate());
}

void Search::on_backButton_clicked()
{
    m_app->showMenu();
}

void Search::on_searchButton_clicked()
{
    // Create the filter from the inputted parameters.
    Filter::Data data;

    if (!titleInput->text().isEmpty()) {
        data.title = QRegularExpression(titleInput->text());
    }
    if (!senderInput->text().isEmpty()) {
        data.senderName = QRegularExpression(senderInput->text());
    }
    if (!filetypeInput->text().isEmpty()) {
        data.fileType = QRegularExpression(filetypeInput->text());
    }
    if (!noteInput->text().isEmpty()) {
        data.note = QRegularExpression(noteInput->text());
    }
    if (startDateCheckBox->isChecked()) {
        data.receiveTimeStart = QDateTime(startDateInput->date(), QTime(0, 0));
    }
    if (endDateCheckBox->isChecked()) {
        data.receiveTimeEnd = QDateTime(endDateInput->date(), QTime(23, 59, 59));
    }
    data.isUnread = isUnreadInput->isChecked();

    // Show the filter list.
    m_app->showList(Filter(data));
}
