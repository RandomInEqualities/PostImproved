#pragma once

#include <QLayout>
#include <QLayoutItem>
#include <QRect>
#include <QSize>
#include <QStyle>
#include <QWidget>

// Layout that lays its item out in a grid, the items flows around when the
// window is resized and adapts the item sizes to fit.
//
// The idea for this class came from the Qt flowlayout example.
class FlowLayout : public QLayout
{
public:
    FlowLayout(QWidget* parent, QSize minItemSize, int margin = -1);
    FlowLayout(QSize minItemSize, int margin = -1);
    ~FlowLayout();

    // Set the minimum size that an item can have.
    void setMinimumItemSize(QSize size);

    // Methods for implementing the flow layout.
    void addItem(QLayoutItem* item) override;
    Qt::Orientations expandingDirections() const override;
    bool hasHeightForWidth() const override;
    int heightForWidth(int) const override;
    int count() const override;
    QLayoutItem* itemAt(int index) const override;
    QSize minimumSize() const override;
    void setGeometry(const QRect& rect) override;
    QSize sizeHint() const override;
    QLayoutItem* takeAt(int index) override;

private:
    int doLayout(const QRect& rect, bool testOnly) const;

    QList<QLayoutItem*> m_itemList;
    QSize m_minItemSize;
};
