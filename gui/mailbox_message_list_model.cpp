#include "mailbox_message_list_model.h"

#include <QColor>
#include <QMessageBox>
#include <QXmlStreamWriter>

MailBoxMessageListModel::MailBoxMessageListModel(QObject* parent, eboks::Account* account, const eboks::Folder& folder)
    : MessageListModel(parent), m_account(account), m_fetchMore(true), m_folder(folder)
{
    connect(m_account, &eboks::Account::messageCreated, this, [=]() { refreshMessageTree(); });
    connect(m_account, &eboks::Account::messageDeleted, this, [=]() { refreshMessageTree(); });
    connect(m_account, &eboks::Account::messageMoved, this, [=]() { refreshMessageTree(); });
    connect(m_account, &eboks::Account::messageUpdated, this, &MailBoxMessageListModel::updateMessage);
}

const eboks::Folder& MailBoxMessageListModel::getFolder() const
{
    return m_folder;
}

QVariant MailBoxMessageListModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    if (role == Qt::DecorationRole && index.column() == Column::title) {
        // TODO: add colour variation based on what?
        return QColor("grey");
    }
    else if (role == Qt::ForegroundRole && getMessage(index).isUnread()) {
        return QColor(Qt::red);
    }
    else if (role == Qt::EditRole) {
        return getMessage(index).getTitle();
    }

    return MessageListModel::data(index, role);
}

Qt::ItemFlags MailBoxMessageListModel::flags(const QModelIndex& index) const
{
    if (!index.isValid()) {
        return Qt::NoItemFlags;
    }

    auto flags = MessageListModel::flags(index) | Qt::ItemIsDragEnabled;

    if (index.column() == Column::title) {
        flags = flags | Qt::ItemIsEditable;
    }

    return flags;
}

bool MailBoxMessageListModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (!index.isValid()) {
        return false;
    }

    if (role == Qt::EditRole) {
        if (index.column() != Column::title) {
            return false;
        }

        QString text = value.toString();
        if (text.isEmpty()) {
            return false;
        }

        eboks::Message message = getMessage(index);
        m_account->setMessageTitle(message.getFolderId(), message.getId(), text);
        emit dataChanged(index, index, {Qt::EditRole});
        return true;
    }

    return false;
}

QStringList MailBoxMessageListModel::mimeTypes() const
{
    return {"text/xml"};
}

QMimeData* MailBoxMessageListModel::mimeData(const QModelIndexList& indices) const
{
    if (indices.isEmpty()) {
        return nullptr;
    }

    QMimeData* mimeData = new QMimeData();
    QByteArray text;
    QXmlStreamWriter writer(&text);

    writer.writeStartElement("Messages");
    for (auto index : indices) {
        Q_ASSERT(index.isValid());

        // The indices list might contain several indices for each row, only
        // send one message for each row.
        if (index.column() != Column::title) {
            continue;
        }

        eboks::Message message = getMessage(index);
        writer.writeStartElement("Message");
        writer.writeAttribute("id", message.getId().toString());
        writer.writeAttribute("folderId", message.getFolderId().toString());
        writer.writeEndElement();
    }
    writer.writeEndElement();

    mimeData->setData("text/xml", text);
    return mimeData;
}

Qt::DropActions MailBoxMessageListModel::supportedDragActions() const
{
    return Qt::MoveAction;
}

bool MailBoxMessageListModel::canFetchMore(const QModelIndex& parent) const
{
    if (parent.isValid()) {
        return false;
    }
    return m_fetchMore;
}

void MailBoxMessageListModel::fetchMore(const QModelIndex& parent)
{
    if (parent.isValid()) {
        return;
    }

    // Check if we already have a fetch request in progress.
    Q_ASSERT(m_fetchMore);
    if (m_requestInProgress) {
        return;
    }

    // Send a fetch request for new messages.
    m_requestInProgress = m_account->getFromFolder(m_folder.getId(), getMessageAmount(), DEFUALT_TAKE);
    connect(m_requestInProgress.get(), &eboks::Request::finished, this, &MailBoxMessageListModel::fetchReply);
}

void MailBoxMessageListModel::reloadMessages()
{
    // Cancel any pending requests.
    if (m_requestInProgress) {
        bool success = disconnect(
            m_requestInProgress.get(), &eboks::Request::finished, this, &MailBoxMessageListModel::fetchReply);
        Q_ASSERT(success);
        Q_UNUSED(success);
        m_requestInProgress = nullptr;
    }

    clearMessages();
    m_fetchMore = true;
}

void MailBoxMessageListModel::fetchReply(QNetworkReply* reply)
{
    Q_ASSERT(m_requestInProgress.get() == QObject::sender());
    m_requestInProgress = nullptr;
    if (reply->error()) {
        return;
    }

    QVector<eboks::Message> messages = m_account->parseMessagesXml(reply);
    addMessages(messages);

    // If eboks returns less than what we requested, we assume that there is
    // no more messages to fetch.
    if (messages.size() < DEFUALT_TAKE) {
        m_fetchMore = false;
    }
}

void MailBoxMessageListModel::refreshMessageTree()
{
    this->reloadMessages();
}
