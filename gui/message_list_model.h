#pragma once

#include "eboks/account.h"
#include "eboks/message.h"

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QSortFilterProxyModel>
#include <QString>
#include <QVariant>
#include <QVector>

// This class represent a list of messages. It allows the messages to be
// displayed in a QAbstractItemView (QTreeView, QListView, etc).
//
// The class should be subclassed to add messages to the model.
class MessageListModel : public QAbstractItemModel
{
public:
    // The columns in the model.
    enum Column
    {
        title = 0,
        sender = 1,
        date = 2,
        note = 3,
        attachmentCount = 4,
        fileSize = 5,
        fileType = 6,
        COUNT = 7
    };

public:
    MessageListModel(QObject* parent);
    virtual ~MessageListModel();

    // Specify the model data.
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Specify the model indices.
    QModelIndex parent(const QModelIndex& index) const override;
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex sibling(int row, int column, const QModelIndex& index) const override;

    // Get the message that a modelindex points to.
    const eboks::Message& getMessage(const QModelIndex& index) const;

protected:
    // Remove all messages in the model. Properly resets the model data.
    void clearMessages();

    // Set the messages in the model. Properly resets the model data.
    void setMessages(const QVector<eboks::Message>& messages);

    // Append messages to the model. Properly updates the model data.
    void addMessages(const QVector<eboks::Message>& messages);

    // Get the amount of messages in the model.
    int getMessageAmount() const;

    // The amount of message that we fetch at a time.
    static constexpr int DEFUALT_TAKE = 200;

protected slots:

    // Update a message's content.
    void updateMessage(eboks::FolderId folderId, eboks::MessageId messageId, eboks::Account::MessageUpdateInfo info);

private:
    // Check if two message vectors have any messages in common.
    static bool haveMessageInCommon(const QVector<eboks::Message>& messagesA, const QVector<eboks::Message>& messagesB);

private:
    QVector<eboks::Message> m_messages;
};

// This class represent a list of messages that can be sorted. It allows the
// messages to be displayed in a QAbstractItemView (QTreeView, QListView, etc.)
class SortedMessageListModel : public QSortFilterProxyModel
{
public:
    SortedMessageListModel(QObject* parent);
    virtual ~SortedMessageListModel();

    // Get the message that a modelindex points to.
    const eboks::Message& getMessage(const QModelIndex& index) const;

    // Set the model that gets sorted.
    void setSourceModel(MessageListModel* model);

private:
    MessageListModel* m_model;
};
