#pragma once

#include "ui_filter_list.h"

#include "app_controller.h"
#include "filter_model.h"
#include "message_list_model.h"

#include "eboks/account.h"
#include "eboks/message.h"

#include <QModelIndex>
#include <QWidget>

class FilterList : public QWidget, private Ui::FilterList
{
private:
    Q_OBJECT

public:
    FilterList(AppController* app, eboks::Account* account);

    void setFilter(const Filter& filter);
    void setBackWidget(QWidget* widget);

private slots:

    void clickedMessage(const QModelIndex& index);
    void fitColumnsToContent(const QModelIndex&, int, int);
    void on_backButton_clicked();

private:
    AppController* m_app;
    eboks::Account* m_account;
    FilterModel* m_model;
    SortedMessageListModel* m_filter;
    QWidget* m_backWidget;
};
