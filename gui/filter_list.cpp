#include "filter_list.h"

#include "tile_menu.h"

FilterList::FilterList(AppController* app, eboks::Account* account)
    : QWidget(app), m_app(app), m_account(account), m_model(nullptr), m_backWidget(nullptr)
{
    setupUi(this);

    // Create the model that sort the items in the tree.
    m_model = new FilterModel(this, m_account);
    m_filter = new SortedMessageListModel(this);
    treeView->setModel(m_filter);

    // fit columns to content when something is inserted in the model.
    connect(m_filter, &SortedMessageListModel::rowsInserted, this, &FilterList::fitColumnsToContent);

    // Show the message that the user clicks on.
    connect(treeView, &QTreeView::clicked, this, &FilterList::clickedMessage);

    // The search input field should filter out messages in the model.
    connect(searchInput, &QLineEdit::textChanged, m_filter, &SortedMessageListModel::setFilterFixedString);
}

void FilterList::setFilter(const Filter& filter)
{
    m_model->setFilter(filter);
    m_filter->setSourceModel(m_model);
}

void FilterList::setBackWidget(QWidget* widget)
{
    m_backWidget = widget;
}

void FilterList::clickedMessage(const QModelIndex& /*index*/)
{
    m_app->showListContent(m_filter, treeView);
}

void FilterList::fitColumnsToContent(const QModelIndex&, int, int)
{
    treeView->resizeColumnToContents(MessageListModel::Column::title);
    treeView->resizeColumnToContents(MessageListModel::Column::sender);
    treeView->resizeColumnToContents(MessageListModel::Column::date);
    treeView->resizeColumnToContents(MessageListModel::Column::attachmentCount);
    treeView->resizeColumnToContents(MessageListModel::Column::fileSize);
    treeView->resizeColumnToContents(MessageListModel::Column::fileType);
    treeView->sortByColumn(MessageListModel::Column::date, Qt::DescendingOrder);

    // This should only be done once. We disconnect the signal after it has
    // been called.
    disconnect(m_filter, &SortedMessageListModel::rowsInserted, this, &FilterList::fitColumnsToContent);
}

void FilterList::on_backButton_clicked()
{
    m_app->showWidget(m_backWidget);
}
