#include "tile_button.h"

TileButton::TileButton(const QString& elidedText, QWidget* parent)
    : QPushButton(elidedText, parent), m_elidedText(elidedText)
{
    setToolTip(m_elidedText);
}

void TileButton::setElidedText(QString text)
{
    m_elidedText = text;
    setToolTip(m_elidedText);
}

void TileButton::paintEvent(QPaintEvent* event)
{
    QFontMetrics metrics(font());
    QString paintText = metrics.elidedText(m_elidedText, Qt::ElideRight, width() - width() / 20);
    setText(paintText);
    QPushButton::paintEvent(event);
}
