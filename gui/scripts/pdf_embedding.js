/*
 * Returns a promise that resolves when condition returns true.
 *
 * Usefull for waiting for some condition, using either .then or await on the promise.
 */
function waitFor(condition) {
    const resolver = resolve => {
        if (condition()) {
            resolve();
        }
        else  {
            const recheckIntervalMs = 1;
            setTimeout(_ => resolver(resolve), recheckIntervalMs);
        }
    };
    return new Promise(resolver);
}

/*
 * Load raw PDF data from the C++ context.
 *
 * Binary data is base64-encoded and has to be decoded back here.
 */
function showPdfFile(b64Data) {
    const raw = window.atob(b64Data);
    const length = raw.length;

    let buffer = new ArrayBuffer(length);
    let array = new Uint8Array(buffer);

    for (var i = 0; i < length; i++) {
        array[i] = raw.charCodeAt(i);
    }

    const viewerIsReady = function() {
        return window.PDFViewerApplication !== undefined;
    };
    const openPdf = function() {
        window.PDFViewerApplication.open(array);
    };
    waitFor(viewerIsReady).then(openPdf);
}

/*
 * Base64 encoded embedded PDF file.
 */
showPdfFile('%1');  // C++ code pastes base64 here.
