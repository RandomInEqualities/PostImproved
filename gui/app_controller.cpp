#include "app_controller.h"

#include "filter_list.h"
#include "login.h"
#include "mailbox.h"
#include "menu.h"
#include "message_list_content.h"
#include "message_list_model.h"
#include "search.h"
#include "tile_menu.h"

#include <QCloseEvent>
#include <QDebug>
#include <QEventLoop>
#include <QMessageBox>
#include <QSettings>

AppController::AppController(QWidget* parent)
    : QStackedWidget(parent),
      login(nullptr),
      isLoggedIn(false),
      session(nullptr),
      account(nullptr),
      menu(nullptr),
      mailbox(nullptr),
      search(nullptr),
      tileMenu(nullptr),
      filterList(nullptr),
      messageListContent(nullptr)
{
    Q_INIT_RESOURCE(images);

    setWindowIcon(QIcon(":/images/logo.png"));

    // Load the window geometry.
    QSettings settings;
    restoreGeometry(settings.value("AppGeometry").toByteArray());

    // Create the e-Boks session handler.
    session = new eboks::Session(this);
    connect(session, &eboks::Session::loginFinished, this, &AppController::loginFinished);
    connect(session, &eboks::Session::logoutFinished, this, &AppController::logoutFinished);
    connect(session, &eboks::Session::error, this, &AppController::networkError);

    // Create the login widget.
    login = new Login(this, session);
    addWidget(login);
    setCurrentWidget(login);
}

void AppController::showLogin()
{
    if (waitForDownloads()) {
        return;
    }

    destroyWidgets();
    session->logout();
    waitForLogout();
}

void AppController::showMenu()
{
    setCurrentWidget(menu);
}

void AppController::showMailBox()
{
    mailbox->prepareShow();
    setCurrentWidget(mailbox);
}

void AppController::showSearch()
{
    setCurrentWidget(search);
}

void AppController::showTileMenu()
{
    setCurrentWidget(tileMenu);
}

void AppController::showList(const Filter& filter)
{
    filterList->setBackWidget(currentWidget());
    filterList->setFilter(filter);
    setCurrentWidget(filterList);
}

void AppController::showListContent(SortedMessageListModel* list, QTreeView* view)
{
    messageListContent->setBackWidget(currentWidget());
    messageListContent->setList(list, view);
    setCurrentWidget(messageListContent);
}

void AppController::showWidget(QWidget* widget)
{
    Q_ASSERT(widget != nullptr);
    Q_ASSERT(indexOf(widget) != -1);

    setCurrentWidget(widget);
}

void AppController::closeEvent(QCloseEvent* event)
{
    // Wait for a proper session logout.
    if (isLoggedIn) {
        if (waitForDownloads()) {
            event->ignore();
            return;
        }

        session->logout();
        waitForLogout();
    }

    QSettings settings;
    settings.setValue("AppGeometry", saveGeometry());

    event->accept();
}

void AppController::loginFinished()
{
    Q_ASSERT(!isLoggedIn);
    isLoggedIn = true;

    // Create an account object that the child ui widgets can use.
    account = new eboks::Account(session);
    downloadManager = new eboks::DownloadManager(account);
    deletionManager = new eboks::DeletionManager(account);

    // Create all the display widgets.
    createWidgets();

    // Here we can tell tile widgets to prepare stuff that requires us to
    // have an account.
    tileMenu->createTiles();
}

void AppController::logoutFinished()
{
    Q_ASSERT(isLoggedIn);
    isLoggedIn = false;

    // Destroy the display widgets.
    destroyWidgets();

    // Destroy the account object.
    delete account;
    account = nullptr;

    // Show the login widget.
    setCurrentWidget(login);
}

void AppController::networkError(QNetworkReply* reply, std::shared_ptr<eboks::Request> request)
{
    // Don't care about errors that result from login attempts. The login
    // gui should handle these.
    if (request->getType() == eboks::Request::Type::LOGIN) {
        return;
    }

    // Don't care about errors that result from not being able to create the
    // deleted items folder.
    QString errorUrl = "0/mail/folder/0?folderName=Deleted Items";
    int errorCode = 401;
    if (reply->request().url().toString().contains(errorUrl) && reply->error() == errorCode) {
        return;
    }

    QMessageBox info(this);

    if (reply->error() < 200) {
        // A network connection error. Show a dialog about this.
        info.setText("Network Connection Error.");
        info.setInformativeText(reply->errorString());
    }
    else {
        // A e-Boks server error or a bad API request.
        info.setText("Api Request Error.");
        info.setInformativeText(reply->errorString());
    }

    info.exec();
}

void AppController::waitForLogout()
{
    QEventLoop loop;
    connect(session, &eboks::Session::logoutFinished, &loop, &QEventLoop::quit);
    loop.exec();
}

bool AppController::waitForDownloads()
{
    if (!downloadManager->isDownloading()) {
        return false;
    }

    auto response = QMessageBox::question(this, "Download in progress...",
        "Download in progress... Do you want to cancel it?", QMessageBox::Yes | QMessageBox::No, QMessageBox::No);

    if (response == QMessageBox::Yes) {
        delete downloadManager;
        downloadManager = new eboks::DownloadManager(account);
        return false;
    }

    return true;
}

void AppController::createWidgets()
{
    Q_ASSERT(isLoggedIn);

    menu = new Menu(this, account);
    addWidget(menu);

    mailbox = new MailBox(this, account, deletionManager, downloadManager);
    addWidget(mailbox);

    search = new Search(this);
    addWidget(search);

    tileMenu = new TileMenu(this, account);
    addWidget(tileMenu);

    filterList = new FilterList(this, account);
    addWidget(filterList);

    messageListContent = new MessageListContent(this, account);
    addWidget(messageListContent);
}

void AppController::destroyWidgets()
{
    destroyWidget(menu);
    menu = nullptr;

    destroyWidget(mailbox);
    mailbox = nullptr;

    destroyWidget(search);
    search = nullptr;

    destroyWidget(tileMenu);
    tileMenu = nullptr;

    destroyWidget(filterList);
    filterList = nullptr;

    destroyWidget(messageListContent);
    messageListContent = nullptr;
}

void AppController::destroyWidget(QWidget* widget)
{
    if (widget != nullptr) {
        removeWidget(widget);
        delete widget;
    }
}
