#pragma once

#include "ui_login.h"

#include "app_controller.h"

#include "eboks/session.h"

#include <QLineEdit>
#include <QNetworkReply>
#include <QValidator>

class AppController;

class Login : public QWidget, private Ui::Login
{
private:
    Q_OBJECT

public:
    Login(AppController* app, eboks::Session* session);

private slots:

    // Called when the response to a login request is received.
    void loginReply(QNetworkReply* reply);

    void on_loginButton_clicked();

private:
    QValidator* constructValidator(QLineEdit* widget, QString regexp);
    bool isValidInput(QString input, QValidator* validator) const;

    void saveLoginCredentials() const;
    void loadLoginCredentials() const;

    void initializeFocus();

    AppController* m_app;
    eboks::Session* m_session;

    QValidator* m_cprValidator;
    QValidator* m_passwordValidator;
    QValidator* m_activationValidator;
};
