#pragma once

#include "message_list_model.h"

#include "eboks/account.h"
#include "eboks/folder.h"
#include "eboks/request.h"

#include <QMimeData>
#include <QModelIndex>
#include <QModelIndexList>
#include <QNetworkReply>
#include <QObject>
#include <QString>
#include <QVariant>
#include <QVector>

class MailBoxMessageListModel : public MessageListModel
{
private:
    Q_OBJECT

public:
    MailBoxMessageListModel(QObject* parent, eboks::Account* account, const eboks::Folder& folder);

    // Get the folder that is displayed.
    const eboks::Folder& getFolder() const;

    // This model has checkboxes that follows the current selection. Clicking
    // on the checkbox icons adds items to the current selection. These methods
    // implement it.
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;

    // The messages can be dragged and dropped, these methods define what
    // messages look like in a drop action.
    QStringList mimeTypes() const override;
    QMimeData* mimeData(const QModelIndexList& indexes) const override;
    Qt::DropActions supportedDragActions() const override;

    // Methods implementing incremental messages fetching.
    bool canFetchMore(const QModelIndex& parent) const override;
    void fetchMore(const QModelIndex& parent) override;

    // Remove all messages in the model and download new ones.
    void reloadMessages();

private slots:

    void fetchReply(QNetworkReply* reply);
    void refreshMessageTree();

private:
    eboks::Account* m_account;
    bool m_fetchMore;
    std::shared_ptr<eboks::Request> m_requestInProgress;

    eboks::Folder m_folder;
};
