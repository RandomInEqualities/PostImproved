#pragma once

#include "eboks/account.h"
#include "eboks/deletion_manager.h"
#include "eboks/downlad_manager.h"
#include "eboks/request.h"
#include "eboks/session.h"

#include <QNetworkReply>
#include <QStackedWidget>
#include <QTreeView>

class Login;
class Menu;
class MailBox;
class Search;
class TileMenu;
class FilterList;
class MessageListContent;
class SortedMessageListModel;
class Filter;

class AppController : public QStackedWidget
{
private:
    Q_OBJECT

public:
    AppController(QWidget* parent = nullptr);

    // Show the login screen. If this is called when a session is already
    // logged in it will logout before showing the screen.
    void showLogin();

    // Show the introduction menu.
    void showMenu();

    // Show the mailbox menu.
    void showMailBox();

    // Show the message search widget.
    void showSearch();

    // Show the tile menu.
    void showTileMenu();

    // Show the list of messages in a tile.
    void showList(const Filter& filter);

    // Show a specific message in a tile.
    void showListContent(SortedMessageListModel* list, QTreeView* view);

    // Show a widget in the window.
    void showWidget(QWidget* widget);

    // Called when then windows closes. Ensures that we logout of the session.
    void closeEvent(QCloseEvent* event) override;

private slots:

    // Slots that are called when the session logs-in and out.
    void loginFinished();
    void logoutFinished();

    // Slot called when the session encounter a network error.
    void networkError(QNetworkReply* reply, std::shared_ptr<eboks::Request> request);

private:
    // Finish all outstanding network requests and log out of the session.
    void waitForLogout();

    // Check if the application should wait for downloads to finish.
    bool waitForDownloads();

    // Create or destroy the managed widgets. This is used to get a clean
    // state after logging out.
    void createWidgets();
    void destroyWidgets();
    void destroyWidget(QWidget* widget);

    Login* login;
    bool isLoggedIn;
    eboks::Session* session;
    eboks::Account* account;
    eboks::DownloadManager* downloadManager;
    eboks::DeletionManager* deletionManager;

    Menu* menu;
    MailBox* mailbox;
    Search* search;
    TileMenu* tileMenu;
    FilterList* filterList;
    MessageListContent* messageListContent;
};
