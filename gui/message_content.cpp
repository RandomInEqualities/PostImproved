#include "message_content.h"

#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QString>
#include <QWebEngineScript>

static QString getFileContent(const QString& fileName)
{
    QFile file(fileName);
    if (file.open(QFile::ReadOnly)) {
        return QString::fromUtf8(file.readAll());
    }
    else {
        qWarning() << fileName << ":" << file.errorString();
        return QString();
    }
}

MessageContentPage::MessageContentPage(QWebEngineProfile* profile) : QWebEnginePage(profile)
{
    connect(this, &MessageContentPage::featurePermissionRequested, this,
        &MessageContentPage::featurePermissionRequestHandle);

    // Macthes background color for the pdf pages.
    setBackgroundColor(QColor(240, 240, 240));
}

void MessageContentPage::featurePermissionRequestHandle(const QUrl& securityOrigin, QWebEnginePage::Feature feature)
{
    setFeaturePermission(securityOrigin, feature, QWebEnginePage::PermissionDeniedByUser);
}

MessageContent::MessageContent(QWidget* parent) : QWebEngineView(parent), m_profile(new QWebEngineProfile())
{
    Q_INIT_RESOURCE(scripts);
    Q_INIT_RESOURCE(pdf_js);

    connect(m_profile, &QWebEngineProfile::downloadRequested, this, &MessageContent::downloadRequested);
}

MessageContent::~MessageContent()
{
    if (page()) {
        page()->deleteLater();
    }
    m_profile->deleteLater(); // must be deleted _after_ all pages are deleted.
}

void MessageContent::render(const eboks::Message& message, const QByteArray& content)
{
    QWebEnginePage* newPage = nullptr;

    const QString filetype = message.getFileType();
    if (filetype == "pdf") {
        newPage = renderPdf(message, content);
    }
    else if (filetype == "htm" || filetype == "html") {
        newPage = renderHtml(message, content);
    }
    else if (filetype == "txt" || filetype == "text") {
        newPage = renderText(message, content);
    }
    else if (filetype == "png" || filetype == "jpeg" || filetype == "jpg") {
        newPage = renderImage(message, content);
    }
    else {
        newPage = renderError(message, content);
    }

    if (page()) {
        page()->deleteLater();
    }
    setPage(newPage);
}

void MessageContent::preRender(const eboks::Message& /*message*/, const QByteArray& /*content*/)
{
    // TODO
}

void MessageContent::downloadRequested(QWebEngineDownloadItem* item)
{
    // The item has a default save path. Let the user choose a location.
    const QString path = QFileDialog::getSaveFileName(this, "Save Document", item->path(), "PDF files (*.pdf)");
    item->setPath(path);
    item->accept();
}

void MessageContent::contextMenuEvent(QContextMenuEvent*)
{
}

QWebEnginePage* MessageContent::renderPdf(const eboks::Message& /*message*/, const QByteArray& content)
{
    auto* page = new MessageContentPage(m_profile);

    connect(page, &MessageContentPage::loadFinished, this, [page, content] {
        const QString pdf = QString::fromUtf8(content.toBase64());
        const QString script = getFileContent(":/scripts/pdf_embedding.js");
        page->runJavaScript(script.arg(pdf));
    });

    page->setUrl(QUrl("qrc:/pdf.js/web/viewer.html"));

    return page;
}

QWebEnginePage* MessageContent::renderHtml(const eboks::Message& /*message*/, const QByteArray& content)
{
    auto* page = new MessageContentPage(m_profile);
    page->setContent(content, "text/html");
    return page;
}

QWebEnginePage* MessageContent::renderText(const eboks::Message& /*message*/, const QByteArray& content)
{
    auto* page = new MessageContentPage(m_profile);
    page->setContent(content);
    return page;
}

QWebEnginePage* MessageContent::renderImage(const eboks::Message& message, const QByteArray& content)
{
    auto* page = new MessageContentPage(m_profile);
    const QString image = QString::fromUtf8(content.toBase64());
    const QString html = QString("<img src='data:image/%1;base64,%2'/>").arg(message.getFileType(), image);
    page->setHtml(html);
    return page;
}

QWebEnginePage* MessageContent::renderError(const eboks::Message& message, const QByteArray& /*content*/)
{
    static const QString html = R"(
        <head></head>
        <body>
            The message could not be decoded.<br/>
            Title: %1 <br/>
            Filetype: %2 <br/>
        </body>
    )";
    auto* page = new MessageContentPage(m_profile);
    page->setHtml(html.arg(message.getTitle().toHtmlEscaped(), message.getFileType().toHtmlEscaped()));
    return page;
}
