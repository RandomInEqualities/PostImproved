# This file compiles the gui source files into a static library.

QT += core gui widgets network webenginewidgets
CONFIG += c++14 static

TARGET = gui
TEMPLATE = lib
DESTDIR = $$OUT_PWD

INCLUDEPATH += ../eboks
DEPENDPATH += ../eboks

SOURCES += \
    app_controller.cpp \
    login.cpp \
    tile_menu.cpp \
    folder_tree_model.cpp \
    message_list_model.cpp \
    menu.cpp \
    mailbox.cpp \
    flowlayout.cpp \
    message_list_content.cpp \
    message_content.cpp \
    mailbox_message_list_model.cpp \
    tile_button.cpp \
    search.cpp \
    filter_model.cpp \
    filter_list.cpp

HEADERS += \
    menu.h \
    app_controller.h \
    login.h \
    mailbox.h \
    tile_menu.h \
    folder_tree_model.h \
    message_list_model.h \
    flowlayout.h \
    message_list_content.h \
    message_content.h \
    mailbox_message_list_model.h \
    tile_button.h \
    search.h \
    filter_model.h \
    filter_list.h

FORMS += \
    menu.ui \
    login.ui \
    mailbox.ui \
    tile_menu.ui \
    message_list_content.ui \
    search.ui \
    filter_list.ui

RESOURCES += \
    pdf.js.qrc \
    images.qrc \
    scripts.qrc

# Javascript files are identified as QML files and can't be loader in QWebEngine.
# https://doc.qt.io/qt-5.11/qtwebengine-deploying.html#javascript-files-in-qt-resource-files
QTQUICK_COMPILER_SKIPPED_RESOURCES += \
    pdf.js.qrc \
    images.qrc
