#include "flowlayout.h"

#include <QtWidgets>

FlowLayout::FlowLayout(QWidget* parent, QSize minItemSize, int margin) : QLayout(parent), m_minItemSize(minItemSize)
{
    setContentsMargins(margin, margin, margin, margin);
}

FlowLayout::FlowLayout(QSize minItemSize, int margin) : FlowLayout(nullptr, minItemSize, margin)
{
}

FlowLayout::~FlowLayout()
{
    QLayoutItem* item;
    while ((item = takeAt(0))) {
        delete item;
    }
}

void FlowLayout::setMinimumItemSize(QSize size)
{
    Q_ASSERT(size.width() >= 0);
    Q_ASSERT(size.height() >= 0);
    m_minItemSize = size;
}

void FlowLayout::addItem(QLayoutItem* item)
{
    Q_ASSERT(item->widget()->minimumWidth() <= m_minItemSize.width());
    Q_ASSERT(item->widget()->minimumHeight() <= m_minItemSize.height());
    m_itemList.append(item);
}

int FlowLayout::count() const
{
    return m_itemList.size();
}

QLayoutItem* FlowLayout::itemAt(int index) const
{
    return m_itemList.value(index);
}

QLayoutItem* FlowLayout::takeAt(int index)
{
    if (index >= 0 && index < m_itemList.size()) {
        return m_itemList.takeAt(index);
    }
    else {
        return 0;
    }
}

Qt::Orientations FlowLayout::expandingDirections() const
{
    return Qt::Vertical | Qt::Horizontal;
}

bool FlowLayout::hasHeightForWidth() const
{
    return true;
}

int FlowLayout::heightForWidth(int width) const
{
    // Needed for widgets to set correct scrollbars.
    int height = doLayout(QRect(0, 0, width, 0), true);
    return height;
}

void FlowLayout::setGeometry(const QRect& rect)
{
    QLayout::setGeometry(rect);
    doLayout(rect, false);
}

QSize FlowLayout::sizeHint() const
{
    return minimumSize();
}

QSize FlowLayout::minimumSize() const
{
    return 2 * m_minItemSize + QSize(2 * margin(), 2 * margin());
}

int FlowLayout::doLayout(const QRect& rectWithMargins, bool testOnly) const
{
    int left, top, right, bottom;
    getContentsMargins(&left, &top, &right, &bottom);
    QRect rect = rectWithMargins.adjusted(+left, +top, -right, -bottom);

    int targetItemWidth = m_minItemSize.width();
    int targetItemHeight = m_minItemSize.height();

    // Find optimal width.
    int allowedDeviation = 20;
    int optimalWidthRows = 8;
    int optimalWidth = rect.width() / optimalWidthRows;
    if (optimalWidth - allowedDeviation > targetItemWidth) {
        targetItemWidth = optimalWidth - allowedDeviation;
        targetItemHeight = optimalWidth - allowedDeviation;
    }

    // Find the space between each item.
    int spaceX = qRound(static_cast<qreal>(targetItemWidth) / 20.0);
    int spaceY = qRound(static_cast<qreal>(targetItemHeight) / 20.0);

    // Find the amount of items in a row.
    int itemsPerRow = rect.width() / (targetItemWidth + spaceX);
    if (itemsPerRow < 1) {
        itemsPerRow = 1;
    }

    // Find the amount of extra space above the target size that each items
    // gets.
    int extraSpacePerRow = rect.width() % (targetItemWidth + spaceX);
    int extraSpacePerItem = qRound(static_cast<qreal>(extraSpacePerRow) / static_cast<qreal>(itemsPerRow));

    // Compute the items locations.
    int x = rect.x();
    int y = rect.y();
    for (QLayoutItem* item : m_itemList) {
        if (!item->widget()->isVisible()) {
            continue;
        }

        // Find where the current elements ends.
        int endX = x + targetItemWidth + extraSpacePerItem;

        // Find where the next elements begins.
        int nextX = endX + spaceX;

        // Check if we need to move the current element down to the next row.
        if (endX > rect.right()) {
            x = rect.x();
            y = y + targetItemHeight + extraSpacePerItem + spaceY;
            nextX = x + targetItemWidth + extraSpacePerItem + spaceX;
        }

        // Set the current elements position.
        if (!testOnly) {
            int width = targetItemWidth + extraSpacePerItem;
            int height = targetItemHeight + extraSpacePerItem;
            item->setGeometry(QRect(QPoint(x, y), QSize(width, height)));
        }

        x = nextX;
    }

    // Return the height of the complete layout, used to set proper scrollbars.
    return y + targetItemHeight + extraSpacePerItem + spaceY;
}
