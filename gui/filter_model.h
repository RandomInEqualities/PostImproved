#pragma once

#include "message_list_model.h"

#include "eboks/account.h"
#include "eboks/id.h"
#include "eboks/message.h"
#include "eboks/request.h"
#include "eboks/sender.h"

#include <QDateTime>
#include <QNetworkReply>
#include <QRegularExpression>
#include <QString>

class Filter
{
public:
    struct Data
    {
        QRegularExpression title = QRegularExpression();
        QRegularExpression senderName = QRegularExpression();
        QRegularExpression fileType = QRegularExpression();
        QRegularExpression note = QRegularExpression();
        QDateTime receiveTimeStart = QDateTime();
        QDateTime receiveTimeEnd = QDateTime();
        bool isUnread = false;
        bool haveAttachments = false;
        eboks::MessageId id = eboks::MessageId();
        eboks::FolderId folderId = eboks::FolderId();
        eboks::SenderId senderId = eboks::SenderId();
    };

    // Filter that accepts all messages.
    Filter();

    // Filter that accepts messages that matches data.
    Filter(const Data& data, QString filterName = "");

    // Get the filter name.
    QString getName() const;

    // See if a message matches the filter.
    bool matches(const eboks::Message& message) const;

private:
    QString filterName;
    Data data;
};

class FilterModel : public MessageListModel
{
private:
    Q_OBJECT

public:
    FilterModel(QObject* parent, eboks::Account* account, Filter filter = Filter());

    // Set the message filter.
    void setFilter(Filter filter);

    // See if the mode can fetch more messages from e-Boks.
    bool canFetchMore(const QModelIndex& parent) const override;
    void fetchMore(const QModelIndex& parent) override;

private slots:

    // Added messages from an e-Boks response to the model.
    void receiveMessages(QNetworkReply* reply);

private:
    // Go through all messages and expose those that match the current filter.
    void reFilterMessages();

    // The filter to apply to all messages.
    Filter m_filter;

    // All message that have been fetched. We only expose the messages that
    // match the current filter.
    QVector<eboks::Message> m_messages;

    // We start fetching from the current date and time, in increments and stop
    // when we have all messages in e-Boks.
    QDateTime m_currentFetch;
    const QDateTime m_lastFetch = QDateTime(QDate(2008, 1, 1), QTime(0, 0));
    const int m_monthsBetweenFetch = -12;

    bool m_fetchMore;
    std::shared_ptr<eboks::Request> m_fetchRequest;
    eboks::Account* m_account;
};
