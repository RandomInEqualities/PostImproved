#include "filter_model.h"

Filter::Filter() : filterName(""), data()
{
}

Filter::Filter(const Filter::Data& data, QString filterName) : filterName(filterName), data(data)
{
}

QString Filter::getName() const
{
    return filterName;
}

bool Filter::matches(const eboks::Message& message) const
{
    if (!data.title.match(message.getTitle()).hasMatch()) {
        return false;
    }
    if (!data.senderName.match(message.getSender().getName()).hasMatch()) {
        return false;
    }
    if (!data.fileType.match(message.getFileType()).hasMatch()) {
        return false;
    }
    if (!data.note.match(message.getNote()).hasMatch()) {
        return false;
    }
    if (data.receiveTimeStart.isValid() && message.getReceiveTime() < data.receiveTimeStart) {
        return false;
    }
    if (data.receiveTimeEnd.isValid() && message.getReceiveTime() > data.receiveTimeEnd) {
        return false;
    }
    if (data.isUnread && !message.isUnread()) {
        return false;
    }
    if (data.haveAttachments && message.getAttachmentCount() == 0) {
        return false;
    }
    if (data.id.isValid() && message.getId() != data.id) {
        return false;
    }
    if (data.folderId.isValid() && message.getFolderId() != data.folderId) {
        return false;
    }
    if (data.senderId.isValid() && message.getSender().getId() != data.senderId) {
        return false;
    }
    return true;
}

FilterModel::FilterModel(QObject* parent, eboks::Account* account, Filter filter)
    : MessageListModel(parent), m_account(account), m_filter(filter), m_fetchMore(true)
{
    m_currentFetch = QDateTime::currentDateTime().addDays(1);
}

void FilterModel::setFilter(Filter filter)
{
    m_filter = filter;
    reFilterMessages();
}

bool FilterModel::canFetchMore(const QModelIndex& parent) const
{
    if (parent.isValid()) {
        return false;
    }
    return m_fetchMore;
}

void FilterModel::fetchMore(const QModelIndex& parent)
{
    if (!m_fetchMore || m_fetchRequest || parent.isValid()) {
        return;
    }

    QDateTime endFetch = m_currentFetch.addDays(-1);
    QDateTime startFetch = m_currentFetch.addMonths(m_monthsBetweenFetch);

    // Adjust the start and end time such that they are 1 millisecond apart and
    // on different days. Prevents duplicate messages being fetched.
    startFetch.setTime(QTime(0, 0, 0, 0));
    endFetch.setTime(QTime(23, 59, 59, 999));

    m_fetchRequest = m_account->getBetweenDates(startFetch, endFetch);
    connect(m_fetchRequest.get(), &eboks::Request::finished, this, &FilterModel::receiveMessages);

    m_currentFetch = startFetch;
}

void FilterModel::receiveMessages(QNetworkReply* reply)
{
    m_fetchRequest = nullptr;
    if (reply->error()) {
        return;
    }

    QVector<eboks::Message> messages = m_account->parseMessagesXml(reply);
    m_messages += messages;

    // Display the messages that matches the filter.
    QVector<eboks::Message> matches;
    for (const auto& message : messages) {
        if (m_filter.matches(message)) {
            matches.append(message);
        }
    }
    addMessages(matches);

    // Fetch more messages if we do not have everything.
    if (m_currentFetch >= m_lastFetch) {
        fetchMore(QModelIndex());
    }
    else {
        m_fetchMore = false;
    }
}

void FilterModel::reFilterMessages()
{
    QVector<eboks::Message> matches;
    for (const auto& message : m_messages) {
        if (m_filter.matches(message)) {
            matches.append(message);
        }
    }
    setMessages(matches);
}
