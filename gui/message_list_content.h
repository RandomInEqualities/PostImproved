#pragma once

#include "ui_message_list_content.h"

#include "app_controller.h"
#include "message_list_model.h"

#include "eboks/account.h"
#include "eboks/id.h"
#include "eboks/message.h"

#include <QByteArray>
#include <QHash>
#include <QModelIndex>
#include <QNetworkReply>
#include <QTreeView>
#include <QWidget>

// Class that displays the messages in a message list model and corresponding
// QTreeView.
class MessageListContent : public QWidget, private Ui::MessageListContent
{
private:
    Q_OBJECT

public:
    MessageListContent(AppController* app, eboks::Account* account);

    // Display a message from a list. It needs the SortedMessageListModel to
    // fetch the messages and the QTreeView to get and modify the currently
    // selected message.
    void setList(SortedMessageListModel* list, QTreeView* view);

    // Set the widget that gets displayed on a back button click.
    void setBackWidget(QWidget* widget);

private:
    // Render a message from the cache. It is rendered either on the display
    // if it is the message we want to display or offscreen if not.
    void renderFromCache(const eboks::Message& message);

    // Set the display message to one next to the currently selected message.
    // Will return false if no message with that offset can be selected.
    bool setDisplayMessage(int selectedMessageOffset);

    // Get a message from e-Boks and render it either onscreen or offscreen.
    void fetchMessage(const eboks::Message& message);

private slots:

    void on_backButton_clicked();
    void on_nextButton_clicked();
    void on_previousButton_clicked();

private:
    AppController* m_app;
    eboks::Account* m_account;
    QWidget* m_backWidget;

    // The list that we fetches messages from.
    SortedMessageListModel* m_list;

    // The view that we finds the selected messages from.
    QTreeView* m_view;

    // The message that we display or want to display once it is downloaded.
    eboks::Message m_displayMessage;

    // A cache of downloaded message so we dont need to download then again.
    QHash<eboks::MessageId, QByteArray> m_contentCache;
};
