#pragma once

#include "ui_mailbox.h"

#include "app_controller.h"
#include "folder_tree_model.h"
#include "mailbox_message_list_model.h"
#include "message_list_model.h"

#include "eboks/account.h"
#include "eboks/deletion_manager.h"
#include "eboks/downlad_manager.h"

#include <QMenu>
#include <QModelIndex>
#include <QPoint>

class MailBox : public QWidget, private Ui::MailBox
{
private:
    Q_OBJECT

public:
    MailBox(AppController* app, eboks::Account* account, eboks::DeletionManager* deletionManager,
        eboks::DownloadManager* downloadManager);

    // Should be called before the widget is shown.
    void prepareShow();

private slots:

    void on_createFolderButton_clicked();
    void on_addNoteButton_clicked();
    void on_downloadMessageButton_clicked();
    void on_downloadFolderButton_clicked();
    void on_uploadFileButton_clicked();
    void on_refreshButton_clicked();
    void on_backButton_clicked();
    void on_deleteMessageButton_clicked();
    void on_deleteFolderButton_clicked();

    void on_folderWidget_clicked(const QModelIndex& index);
    void on_messageWidget_doubleClicked(const QModelIndex& index);

    void renameSelectedMessages();
    void renameSelectedFolder();

    void contextualMenuFolder(const QPoint& point);
    void messageContextMenuRequested(const QPoint& point);

    void showInboxMessages();
    void fitColumnsToContent(const QModelIndex&, int, int);

private:
    const qint64 MAX_UPLOAD_BYTESIZE = 10000000;

    AppController* app;
    eboks::Account* account;
    eboks::DeletionManager* deletionManager;
    eboks::DownloadManager* downloadManager;

    FolderTreeModel* folderModel;
    MailBoxMessageListModel* messageListModel;
    SortedMessageListModel* messageFilter;

    QMenu* folderMenu;
    QMenu* messageMenu;

private:
    void setupMenus();
};
