#pragma once

#include <QPushButton>

class TileButton : public QPushButton
{
public:
    TileButton(const QString& elidedText, QWidget* parent);

    // Set text that should be fitted into the button and ellided with ...
    // if it is too long.
    void setElidedText(QString text);

    // Called when the widget should be painted.
    void paintEvent(QPaintEvent* event) override;

private:
    QString m_elidedText;
};
