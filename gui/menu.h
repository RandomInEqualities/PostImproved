#pragma once

#include "ui_menu.h"

#include "app_controller.h"

#include "eboks/account.h"

#include <QWidget>

class Menu : public QWidget, private Ui::Menu
{
    Q_OBJECT

public:
    Menu(AppController* app, eboks::Account* account);

private slots:

    void on_mailboxButton_clicked();
    void on_tilemenuButton_clicked();
    void on_logoutButton_clicked();

    void on_searchButton_clicked();

private:
    AppController* m_app;
    eboks::Account* m_account;
};
