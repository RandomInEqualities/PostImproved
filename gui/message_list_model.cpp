#include "message_list_model.h"

MessageListModel::MessageListModel(QObject* parent) : QAbstractItemModel(parent)
{
}

MessageListModel::~MessageListModel()
{
}

int MessageListModel::rowCount(const QModelIndex& parent) const
{
    // No valid modelindices have childs because this is a list.
    if (parent.isValid()) {
        return 0;
    }
    return m_messages.size();
}

int MessageListModel::columnCount(const QModelIndex& parent) const
{
    // No valid modelindices have childs because this is a list.
    if (parent.isValid()) {
        return 0;
    }
    return Column::COUNT;
}

QVariant MessageListModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }
    Q_ASSERT(index.column() >= 0 && index.column() < Column::COUNT);
    Q_ASSERT(index.row() >= 0 && index.row() < m_messages.size());

    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    eboks::Message message = getMessage(index);
    switch (index.column()) {
    case Column::title:
        return message.getTitle();
    case Column::sender:
        return message.getSender().getName();
    case Column::date:
        return message.getReceiveTime();
    case Column::note:
        return message.getNote();
    case Column::attachmentCount:
        return message.getAttachmentCount();
    case Column::fileSize:
        return QString::number(message.getFileSize() / 1000.0, 'f', 1);
    case Column::fileType:
        return message.getFileType();
    default:
        Q_ASSERT(false);
        return QVariant();
    }
}

QVariant MessageListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation);

    if (role != Qt::DisplayRole) {
        return QVariant();
    }

    if (orientation != Qt::Horizontal) {
        return QVariant();
    }

    switch (section) {
    case Column::title:
        return "Title";
    case Column::sender:
        return "Sender";
    case Column::date:
        return "Date";
    case Column::note:
        return "Note";
    case Column::attachmentCount:
        return "Attachments";
    case Column::fileSize:
        return "Size (kilobytes)";
    case Column::fileType:
        return "File type";
    default:
        Q_ASSERT(false);
        return QVariant();
    }
}

Qt::ItemFlags MessageListModel::flags(const QModelIndex& index) const
{
    if (!index.isValid()) {
        return Qt::NoItemFlags;
    }
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemNeverHasChildren;
}

QModelIndex MessageListModel::parent(const QModelIndex& index) const
{
    // This model is a one dimensional list with no parents.
    Q_UNUSED(index);
    return QModelIndex();
}

QModelIndex MessageListModel::index(int row, int column, const QModelIndex& parent) const
{
    // No valid modelindices have childs because this is a list.
    if (parent.isValid()) {
        return QModelIndex();
    }
    if (row < 0 || row >= m_messages.size()) {
        return QModelIndex();
    }
    if (column < 0 || column >= Column::COUNT) {
        return QModelIndex();
    }
    return createIndex(row, column);
}

QModelIndex MessageListModel::sibling(int row, int column, const QModelIndex& index) const
{
    if (!index.isValid()) {
        return QModelIndex();
    }
    return MessageListModel::index(row, column);
}

const eboks::Message& MessageListModel::getMessage(const QModelIndex& index) const
{
    Q_ASSERT(index.isValid());
    Q_ASSERT(index.row() >= 0);
    Q_ASSERT(index.row() < m_messages.size());
    Q_ASSERT(index.model() == this);
    return m_messages[index.row()];
}

void MessageListModel::clearMessages()
{
    setMessages(QVector<eboks::Message>());
}

void MessageListModel::addMessages(const QVector<eboks::Message>& messages)
{
    if (messages.isEmpty())
        return;

    int rows = rowCount();
    beginInsertRows(QModelIndex(), rows, rows + messages.size() - 1);
    Q_ASSERT(!haveMessageInCommon(m_messages, messages));
    m_messages += messages;
    endInsertRows();
}

void MessageListModel::setMessages(const QVector<eboks::Message>& messages)
{
    beginResetModel();
    m_messages = messages;
    endResetModel();
}

int MessageListModel::getMessageAmount() const
{
    return m_messages.size();
}

void MessageListModel::updateMessage(
    eboks::FolderId folderId, eboks::MessageId messageId, eboks::Account::MessageUpdateInfo info)
{
    Q_UNUSED(folderId);

    for (int messageIndex = 0; messageIndex < m_messages.size(); messageIndex++) {
        auto& message = m_messages[messageIndex];

        if (message.getId() != messageId) {
            continue;
        }

        // Get the updated message data.
        eboks::Message::Data data = message.getData();
        if (info.newTitle.isValid()) {
            data.title = info.newTitle.toString();
        }
        if (info.newNote.isValid()) {
            data.note = info.newNote.toString();
        }
        if (info.newIsUnread.isValid()) {
            data.isUnread = info.newIsUnread.toBool();
        }

        // Replace message.
        message = eboks::Message(data);
        emit dataChanged(index(messageIndex, 0), index(messageIndex, Column::COUNT - 1));
        break;
    }
}

bool MessageListModel::haveMessageInCommon(
    const QVector<eboks::Message>& messagesA, const QVector<eboks::Message>& messagesB)
{
    for (const auto& message : messagesA) {
        if (messagesB.contains(message)) {
            return true;
        }
    }
    return false;
}

SortedMessageListModel::SortedMessageListModel(QObject* parent) : QSortFilterProxyModel(parent), m_model(nullptr)
{
    setFilterCaseSensitivity(Qt::CaseInsensitive);
}

const eboks::Message& SortedMessageListModel::getMessage(const QModelIndex& index) const
{
    Q_ASSERT(m_model != nullptr);
    Q_ASSERT(index.isValid());
    Q_ASSERT(index.model() == this);
    QModelIndex messageIndex = mapToSource(index);
    return m_model->getMessage(messageIndex);
}

SortedMessageListModel::~SortedMessageListModel()
{
}

void SortedMessageListModel::setSourceModel(MessageListModel* model)
{
    QSortFilterProxyModel::setSourceModel(model);
    m_model = model;
}
