#include "tile_menu.h"

#include "tile_button.h"
#include "tile_menu.h"

#include "eboks/folder.h"
#include "eboks/message.h"
#include "eboks/request.h"
#include "eboks/sender.h"

TileMenu::TileMenu(AppController* parent, eboks::Account* account) : QWidget(parent), m_app(parent), m_account(account)
{
    setupUi(this);
    tileView->setLayout(new FlowLayout(m_minimumTileSize));

    connect(searchInput, &QLineEdit::textChanged, this, &TileMenu::searchForTileTextChanged);
}

void TileMenu::tileButtonClicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    m_app->showList(m_tiles[button]);
}

void TileMenu::on_backButton_clicked()
{
    m_app->showMenu();
}

void TileMenu::createFolderTiles(QNetworkReply* reply)
{
    if (reply->error()) {
        return;
    }

    auto folders = m_account->parseFoldersXml(reply);
    for (const auto& folder : folders) {
        Filter::Data data;
        data.folderId = folder.getId();
        addTileButton(Filter(data, "F: " + folder.getName()));
    }
}

void TileMenu::createSenderTiles(QNetworkReply* reply)
{
    if (reply->error()) {
        return;
    }

    auto senders = m_account->parseSendersXml(reply);
    for (const auto& sender : senders) {
        Filter::Data data;
        data.senderId = sender.getId();
        addTileButton(Filter(data, "S: " + sender.getName()));
    }
}

void TileMenu::searchForTileTextChanged(QString text)
{
    for (QPushButton* button : m_tiles.keys()) {
        bool match = m_tiles.value(button).getName().contains(text, Qt::CaseInsensitive);
        button->setVisible(match);
    }
}

void TileMenu::createEverythingTile()
{
    addTileButton(Filter(Filter::Data(), "Everything"));
}

void TileMenu::createUnreadTile()
{
    Filter::Data data;
    data.isUnread = true;
    addTileButton(Filter(data, "Unread"));
}

void TileMenu::createDateTiles()
{
    auto createDateTile = [&](int startYear, int endYear) {
        Filter::Data data;

        data.receiveTimeStart = QDateTime(QDate(startYear, 1, 1), QTime(0, 0));
        Q_ASSERT(data.receiveTimeStart.isValid());

        data.receiveTimeEnd = QDateTime(QDate(endYear, 1, 1), QTime(0, 0));
        Q_ASSERT(data.receiveTimeEnd.isValid());

        addTileButton(Filter(data, "D: " + QString::number(startYear)));
    };

    QDate date = QDate::currentDate();
    createDateTile(date.year(), date.year() + 1);
    createDateTile(date.year() - 1, date.year());
    createDateTile(date.year() - 2, date.year() - 1);
}

void TileMenu::createTiles()
{
    createEverythingTile();
    createUnreadTile();
    createDateTiles();

    auto folders = m_account->getFolders();
    connect(folders.get(), &eboks::Request::finished, this, &TileMenu::createFolderTiles);

    auto senders = m_account->getSenders();
    connect(senders.get(), &eboks::Request::finished, this, &TileMenu::createSenderTiles);
}

void TileMenu::addTileButton(const Filter& filter)
{
    QPushButton* button = new TileButton(filter.getName(), this);
    connect(button, &QPushButton::clicked, this, &TileMenu::tileButtonClicked);

    button->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    button->setFocusPolicy(Qt::FocusPolicy::NoFocus);

    tileView->layout()->addWidget(button);
    m_tiles[button] = filter;
}
