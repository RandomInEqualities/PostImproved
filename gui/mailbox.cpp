#include "mailbox.h"

#include <QFileDialog>
#include <QInputDialog>
#include <QItemSelection>
#include <QMessageBox>
#include <QVariant>

MailBox::MailBox(AppController* app, eboks::Account* account, eboks::DeletionManager* deletionManager,
    eboks::DownloadManager* downloadManager)
    : QWidget(app),
      app(app),
      account(account),
      deletionManager(deletionManager),
      downloadManager(downloadManager),
      folderModel(nullptr),
      messageListModel(nullptr),
      messageFilter(nullptr),
      folderMenu(nullptr),
      messageMenu(nullptr)
{
    setupUi(this);

    // Set up the message filter model.
    messageFilter = new SortedMessageListModel(this);
    messageWidget->setModel(messageFilter);
    connect(searchInput, &QLineEdit::textChanged, messageFilter, &SortedMessageListModel::setFilterFixedString);

    // Fit columns to content when something is inserted in the message model.
    connect(messageFilter, &SortedMessageListModel::rowsInserted, this, &MailBox::fitColumnsToContent);

    setupMenus();
}

void MailBox::prepareShow()
{
    if (folderModel != nullptr) {
        folderModel->deleteLater();
    }

    folderModel = new FolderTreeModel(this, account);
    folderWidget->setModel(folderModel);

    connect(folderModel, &FolderTreeModel::modelReset, this, &MailBox::showInboxMessages);
}

void MailBox::on_refreshButton_clicked()
{
    messageListModel->reloadMessages();
}

void MailBox::on_folderWidget_clicked(const QModelIndex& index)
{
    if (!index.isValid()) {
        return;
    }

    if (messageListModel != nullptr) {
        messageListModel->deleteLater();
    }

    eboks::Folder folder = folderModel->getFolder(index);
    messageListModel = new MailBoxMessageListModel(this, account, folder);
    messageFilter->setSourceModel(messageListModel);
}

void MailBox::on_downloadFolderButton_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, "Choose Download Folder");
    if (!directory.isEmpty()) {
        QModelIndex index = folderWidget->selectionModel()->currentIndex();
        downloadManager->downloadFolder(folderModel->getFolder(index), QDir(directory));
    }
}

void MailBox::on_downloadMessageButton_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, "Choose Download Folder");
    if (directory.isEmpty()) {
        return;
    }

    QModelIndexList indexes = messageWidget->selectionModel()->selectedRows();
    for (QModelIndex index : indexes) {
        eboks::Message message = messageFilter->getMessage(index);
        downloadManager->downloadMessage(message, QDir(directory));
    }
}

void MailBox::on_messageWidget_doubleClicked(const QModelIndex& index)
{
    eboks::Message message = messageFilter->getMessage(index);
    if (message.isUnread()) {
        account->setMessageRead(message.getFolderId(), message.getId());
    }
    app->showListContent(messageFilter, messageWidget);
}

void MailBox::on_createFolderButton_clicked()
{
    bool confirmed = false;
    QString name = QInputDialog::getText(this, "Create Folder With Name:", "Name:", QLineEdit::Normal, "", &confirmed);

    if (confirmed && !name.isEmpty()) {
        QModelIndex index = folderWidget->selectionModel()->currentIndex();

        eboks::FolderId parentId = eboks::rootId;
        if (index.isValid()) {
            parentId = folderModel->getFolder(index).getId();
        }

        account->createFolder(name, parentId);
    }
}

void MailBox::on_uploadFileButton_clicked()
{
    if (messageListModel == nullptr) {
        QMessageBox::information(this, "Select Folder", "Select Folder before uploading file.", QMessageBox::Ok);
        return;
    }

    QStringList filepaths = QFileDialog::getOpenFileNames(this, "Select Files");
    QStringList exceededSize;
    for (QString filepath : filepaths) {
        QFileInfo file(filepath);

        if (file.size() >= MAX_UPLOAD_BYTESIZE) {
            exceededSize += file.fileName();
            continue;
        }

        int dotIndex = file.fileName().lastIndexOf(".");

        QString type = file.fileName().mid(dotIndex + 1);
        QString name = file.fileName().mid(0, dotIndex);

        if (type.isEmpty()) {
            type = "txt";
        }
        if (name.isEmpty()) {
            name = " ";
        }

        account->uploadFile(messageListModel->getFolder().getId(), name, filepath, type);
    }

    if (!exceededSize.isEmpty()) {
        QString exceedNames;
        for (auto name : exceededSize) {
            exceedNames += ", " + name;
        }

        QMessageBox::information(this, "Exceeded File Size",
            "Some messages was not uploaded because they were too big: " + exceedNames, QMessageBox::Ok);
    }
}

void MailBox::on_deleteMessageButton_clicked()
{
    QModelIndexList indexes = messageWidget->selectionModel()->selectedRows();
    for (QModelIndex index : indexes) {
        eboks::Message message = messageFilter->getMessage(index);
        deletionManager->deleteMessage(message);
    }
}

void MailBox::on_backButton_clicked()
{
    app->showMenu();
}

void MailBox::on_addNoteButton_clicked()
{
    QModelIndexList indexes = messageWidget->selectionModel()->selectedIndexes();

    QString existingNote = "";
    if (indexes.size() == 1) {
        existingNote = messageFilter->getMessage(indexes.at(0)).getNote();
    }

    bool confirmed = false;
    QString note =
        QInputDialog::getText(this, "Set Message Note", "Text:", QLineEdit::Normal, existingNote, &confirmed);

    if (confirmed) {
        if (note.isEmpty()) {
            note = " ";
        }

        for (QModelIndex index : indexes) {
            eboks::Message message = messageFilter->getMessage(index);
            account->setMessageNote(message.getFolderId(), message.getId(), note);
        }
    }
}

void MailBox::on_deleteFolderButton_clicked()
{
    QModelIndex index = folderWidget->selectionModel()->currentIndex();
    eboks::Folder folder = folderModel->getFolder(index);

    if (!folder.isCustomFolder() || folder == deletionManager->getDeletionFolder()) {
        QMessageBox::information(this, "Folder Deleted Not Allowed",
            "The folder is a default e-Boks Folder. It can't be deleted.", QMessageBox::Ok);
        return;
    }

    deletionManager->deleteFolder(folder);
}

void MailBox::renameSelectedMessages()
{
    QModelIndexList indexes = messageWidget->selectionModel()->selectedRows();

    bool confirmed = false;
    QString newName = QInputDialog::getText(this, "Rename Message to:", "Text:", QLineEdit::Normal, "", &confirmed);

    if (confirmed && !newName.isEmpty()) {
        for (QModelIndex index : indexes) {
            messageFilter->setData(index, newName, Qt::EditRole);
        }
    }
}

void MailBox::renameSelectedFolder()
{
    QModelIndex index = folderWidget->selectionModel()->currentIndex();

    bool confirmed = false;
    QString newName = QInputDialog::getText(this, "Rename folder to:", "Text:", QLineEdit::Normal, "", &confirmed);
    if (confirmed && !newName.isEmpty()) {
        eboks::Folder folder = folderModel->getFolder(index);
        account->updateFolder(folder.getId(), folder.getParentId(), newName);
    }
}

void MailBox::contextualMenuFolder(const QPoint& /*point*/)
{
    folderMenu->exec(QCursor::pos());
}

void MailBox::messageContextMenuRequested(const QPoint& /*point*/)
{
    messageMenu->exec(QCursor::pos());
}

void MailBox::showInboxMessages()
{
    // This function selects the inbox folder and shows it in the message view,
    // if there is nothing there.
    if (messageListModel != nullptr) {
        return;
    }

    int row = 0;
    int column = 0;
    while (true) {
        QModelIndex index = folderModel->index(row, column);
        if (!index.isValid()) {
            return;
        }

        eboks::Folder folder = folderModel->getFolder(index);
        if (folder.getType() == eboks::Folder::Type::Inbox) {
            folderWidget->selectionModel()->select(index, QItemSelectionModel::SelectCurrent);
            on_folderWidget_clicked(index);
            return;
        }

        row++;
    }
}

void MailBox::fitColumnsToContent(const QModelIndex&, int, int)
{
    messageWidget->resizeColumnToContents(MessageListModel::Column::title);
    messageWidget->resizeColumnToContents(MessageListModel::Column::sender);
    messageWidget->resizeColumnToContents(MessageListModel::Column::date);
    messageWidget->resizeColumnToContents(MessageListModel::Column::attachmentCount);
    messageWidget->resizeColumnToContents(MessageListModel::Column::fileSize);
    messageWidget->resizeColumnToContents(MessageListModel::Column::fileType);
    messageWidget->sortByColumn(MessageListModel::Column::date, Qt::DescendingOrder);

    // This should only be done once. We disconnect the signal after it has
    // been called.
    disconnect(messageFilter, &SortedMessageListModel::rowsInserted, this, &MailBox::fitColumnsToContent);
}

void MailBox::setupMenus()
{
    // Create message menu.
    messageMenu = new QMenu(this);
    messageMenu->addAction(QString("Download"), this, SLOT(on_downloadMessageButton_clicked()));
    messageMenu->addAction(QString("Delete"), this, SLOT(on_deleteMessageButton_clicked()));
    messageMenu->addAction(QString("Add note"), this, SLOT(on_addNoteButton_clicked()));
    messageMenu->addAction(QString("Rename"), this, SLOT(renameSelectedMessages()));

    messageWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(messageWidget, &QTreeView::customContextMenuRequested, this, &MailBox::messageContextMenuRequested);

    // Create folder menu.
    folderMenu = new QMenu(this);
    folderMenu->addAction(QString("Create"), this, SLOT(on_createFolderButton_clicked()));
    folderMenu->addAction(QString("Download"), this, SLOT(on_downloadFolderButton_clicked()));
    folderMenu->addAction(QString("Delete"), this, SLOT(on_deleteFolderButton_clicked()));
    folderMenu->addAction(QString("Rename"), this, SLOT(renameSelectedFolder()));

    folderWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(folderWidget, &QTreeView::customContextMenuRequested, this, &MailBox::contextualMenuFolder);
}
