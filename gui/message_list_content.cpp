#include "message_list_content.h"

#include "eboks/request.h"

#include <QFileDialog>

MessageListContent::MessageListContent(AppController* app, eboks::Account* account)
    : QWidget(app), m_app(app), m_account(account), m_backWidget(nullptr), m_list(nullptr), m_view(nullptr)
{
    setupUi(this);
}

void MessageListContent::setList(SortedMessageListModel* list, QTreeView* view)
{
    m_list = list;
    m_view = view;

    bool success = setDisplayMessage(0);
    Q_ASSERT(success);
    Q_UNUSED(success);
}

void MessageListContent::setBackWidget(QWidget* widget)
{
    m_backWidget = widget;
}

void MessageListContent::fetchMessage(const eboks::Message& message)
{
    eboks::MessageId id = message.getId();
    if (!m_contentCache.contains(id)) {
        // Download message.
        auto request = m_account->getMessageContent(message.getFolderId(), id);

        // Render the message once downloaded.
        connect(request.get(), &eboks::Request::finished, this, [this, id, message](QNetworkReply* reply) {
            if (reply->error()) {
                return;
            }
            m_contentCache[id] = reply->readAll();
            renderFromCache(message);
        });
    }
    else {
        renderFromCache(message);
    }
}

void MessageListContent::renderFromCache(const eboks::Message& message)
{
    eboks::MessageId id = message.getId();
    if (id == m_displayMessage.getId()) {
        renderView->render(message, m_contentCache[id]);
        titleLabel->setText(message.getTitle());
    }
    else {
        renderView->preRender(message, m_contentCache[id]);
    }
}

void MessageListContent::on_backButton_clicked()
{
    Q_ASSERT(m_backWidget != nullptr);
    m_app->showWidget(m_backWidget);
}

void MessageListContent::on_nextButton_clicked()
{
    bool success = setDisplayMessage(1);
    Q_ASSERT(success);
    Q_UNUSED(success);
}

void MessageListContent::on_previousButton_clicked()
{
    bool success = setDisplayMessage(-1);
    Q_ASSERT(success);
    Q_UNUSED(success);
}

bool MessageListContent::setDisplayMessage(int selectedMessageOffset)
{
    // Find the selected message.
    QModelIndex selected = m_view->currentIndex();
    Q_ASSERT(selected.isValid());

    // Find the display message next to the selected one.
    QModelIndex index = selected.sibling(selected.row() + selectedMessageOffset, selected.column());

    if (!index.isValid()) {
        return false;
    }

    // Select the display message and show it.
    m_displayMessage = m_list->getMessage(index);
    m_view->setCurrentIndex(index);
    fetchMessage(m_displayMessage);

    // Fetch messages next to the selected one. So they can be shown fast.
    QModelIndex next = index.sibling(index.row() + 1, index.column());
    if (next.isValid()) {
        fetchMessage(m_list->getMessage(next));
    }

    QModelIndex previous = index.sibling(index.row() - 1, index.column());
    if (previous.isValid()) {
        fetchMessage(m_list->getMessage(previous));
    }

    QModelIndex nextnext = index.sibling(index.row() + 2, index.column());
    if (nextnext.isValid()) {
        fetchMessage(m_list->getMessage(nextnext));
    }

    QModelIndex previousprevious = index.sibling(index.row() - 2, index.column());
    if (previousprevious.isValid()) {
        fetchMessage(m_list->getMessage(previousprevious));
    }

    // Enable or disable the next and previous buttons if there are no more
    // messages.
    nextButton->setEnabled(next.isValid());
    previousButton->setEnabled(previous.isValid());

    return true;
}
