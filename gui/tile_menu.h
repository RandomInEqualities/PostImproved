#pragma once

#include "ui_tile_menu.h"

#include "app_controller.h"
#include "filter_model.h"
#include "flowlayout.h"

#include "eboks/account.h"

#include <QMap>
#include <QNetworkReply>
#include <QPushButton>
#include <QWidget>

// Class that display a list of tiles corresponding to groups of mail. Each
// tile is a button and a associated filter. The filter can be applied to a
// message list for display.
class TileMenu : public QWidget, private Ui::TileMenu
{
    Q_OBJECT

public:
    TileMenu(AppController* parent, eboks::Account* account);

    void createTiles();

private slots:

    void tileButtonClicked();
    void on_backButton_clicked();
    void searchForTileTextChanged(QString text);

    void createFolderTiles(QNetworkReply* reply);
    void createSenderTiles(QNetworkReply* reply);

private:
    void createEverythingTile();
    void createUnreadTile();
    void createDateTiles();
    void addTileButton(const Filter& filter);

    const QSize m_minimumTileSize = QSize(200, 200);

    AppController* m_app;
    eboks::Account* m_account;
    QMap<QPushButton*, Filter> m_tiles;
};
